(function ( $ ){
  $.fn.my_plugins = function(){
    this.datatable = function(options,colHide=[]){
      if($(this.selector).length>0)
      return $(this.selector).datatable(options,colHide);
    }
    this.page = function(options=null){
      if($(this.selector).length>0)
      return $(this.selector).page(options);
    }
    this.datepicker = function(options=null){
      if($(this.selector).length>0 && !$(this.selector).hasClass('inited')){
        $_dp = $(this.selector).datepicker({startView: 1,format:'dd/mm/yyyy',autoclose:true,todayBtn:'linked'});
        if($(this.selector).val().length>0)
        $_dp.datepicker('update',date_jsFromMysql($(this.selector).val()));
        $(this.selector).addClass('inited');
        return $_dp; 
      }
    }
    this.timepicker = function(options=null){
      if($(this.selector).length>0){
        $_dp = $(this.selector).datetimepicker({format:'HH:mm',showClear:true,allowInputToggle:true});
        return $_dp; 
      }
    }
    this.switch = function(options=null){
      if($(this.selector).length>0)
      return $(this.selector).bootstrapToggle();
    }
    this.form = function(options=null){
      if($(this.selector).length>0)
      return $(this.selector).form().init();
    }
    this.icheck = function(options=null){
      if($(this.selector).length>0 && $(this.selector).attr('type')=='radio')
        return $(this.selector).iCheck({radioClass:"iradio_flat-blue"});
      else if($(this.selector).length>0 && $(this.selector).attr('type')=='checkbox')
        return $(this.selector).iCheck({checkboxClass:"icheckbox_flat-blue"});
    }
    this.select2 = function(options={}){
      if($(this.selector).length>0)
      return $(this.selector).select2(options);
    }
    this.adminpanel = function(){
      if($(this.selector).length>0){
        $(this.selector).find('.panel').attr('data-panel-remove','false').attr('data-panel-title','false');
        return $(this.selector).closest('div').adminpanel().init(); 
      }
    }
    this.magnific = function(){
      $(this.selector).find('img').magnificPopup({
        type: 'image',
        callbacks: {
          beforeOpen: function(e) {
            $('body').addClass('mfp-bg-open');

            this.st.mainClass = 'mfp-zoomIn';

            this.contentContainer.addClass('mfp-with-anim');
          },
          afterClose: function(e) {

            setTimeout(function() {
              $('body').removeClass('mfp-bg-open');
              $(window).trigger('resize');
            }, 1000)

          },
          elementParse: function(item) { console.log(item);
            item.src = item.el.attr('src');
          },
        },
        overflowY: 'scroll',
        removalDelay: 200, //delay removal by X to allow out-animation
        prependTo: $('#content_wrapper')
      });
    }
    this.spinner = function(options = null){
      $(this.selector).spinner(options);
    }
    this.money = function(){
        var _val = $(this).attr('data-value');
        $(this).val(number_to_currency(_val));
    }
    this.summernote = function(){
        if($(this.selector).length>0)
        return $(this.selector).summernote({
            height:'200px',
            focus: false,
            dialogsInBody : true,
            toolbar:[
                ['style', ['style']],
                ['0', ['undo','redo']],
                ['1', ['bold','italic','underline','clear']],
                ['2', ['fontname','fontsize','color']],
                ['3', ['ol','ul','paragraph']],
                ['4', ['table','link','picture','video','hr']],
                ['5', ['fullscreen','codeview']]
            ],
            oninit: function() {},
            onChange: function(contents, $editable) {},
        });
    }
    this.optuserdefined = function(){
        if($(this.selector).length>0)
        return $(this.selector).nestable({maxDepth : 1,group:$(this.selector).attr('name')});
    }
    return this;
  }
}(jQuery));

(function ( $ ){
  $.fn.form = function(){
    var _this=this;
    this.init = function(){
      $(this.selector).find(".my-date-picker, .my-daterange-picker").each(function(){
        $(_this.selector+' input[name="'+$(this).attr('name')+'"]').my_plugins().datepicker();           
      });
      $(this.selector).find(".my-time-picker, div.my-timerange-picker").each(function(){
        $(this.selector).find('#'+$(this).attr('id')).my_plugins().timepicker();        
      });
      $(this.selector).find(".toggle").my_plugins().switch();
      $(this.selector).find('input[type="radio"].my-icheck').my_plugins().icheck();
      $(this.selector).find('input[type="checkbox"].my-icheck').my_plugins().icheck();
      $(this.selector).find(".my-select2").each(function(){
        var opt = $(this).attr('data-options');
        if(opt!=null) opt=JSON.parse(urldecode(opt));
        var $sel = $(this);
        var isdefval = false;
        var _conf = $(this).attr('data-config');
        var conf = {};
        
        var maxsel = $(this).attr('data-selected-max');
        if(typeof maxsel!='undefined') opt.maximumSelectionLength = maxsel;
        
        if(typeof _conf != 'undefined' && _conf != null) var conf = JSON.parse(urldecode(_conf));
        if($(this).find('option:not([value=""])').length==1 && $(this).attr('required')=='required'){
          isdefval = true;
        }
        $(this.selector).find('select[name="'+$(this).attr('name')+'"]').my_plugins().select2(opt);
        
        var _val = $sel.attr('data-value');
        
        if(isdefval) $sel.val($sel.find('option:not([value=""])').attr('value')).trigger('change.select2');
        else if($sel.attr('data-value') != '' && typeof _val != 'undefined'){
            if(is_validJson(urldecode(_val)) && $sel.prop('multiple')) $sel.val(JSON.parse(urldecode(_val))).trigger('change.select2');
            else $sel.val(_val).trigger('change.select2');
        }else $sel.val(null).trigger('change.select2');
        
        if(typeof conf.inited != 'undefined' && conf.inited != null && typeof window[conf.inited] == 'function'){
          window[conf.inited]($(this));
        }
      })
      $(this.selector).find(".dropzone").each(function(){
        var _dzconf = JSON.parse(urldecode($(this).attr('data-config')));
        _dzconf['init'] = function(){
          var $tb = $(this.element).closest('div.dz_container').find('.dz_nestable_files').nestable({
            maxDepth : 1
          });
          $(this.element).closest('div.dz_container').find('.dz_nestable_files ol li').each(function(){
            $("#dz_file_"+$(this).attr('data-id')).fancybox({
                helpers : { title : { type : 'over'}}
            });            
          })
          this.on('success',function(file,param){
            var file = JSON.parse(param);
            var $ol = $(this.element).closest('div.dz_container').find('.dz_nestable_files ol');
            var $li = $('<li>').addClass('dd-item dd-dz-item').attr('data-id',file.id).append(
              `<div class="dd-handle">
                <i class="fa fa-ellipsis-v"></i>&nbsp;&nbsp;
                <span><strong>`+file.filename+`</strong>&nbsp;&nbsp;&nbsp; <small><i class="fa fa-calendar"></i>&nbsp;`+file.uploaddate+`</small></span>
              </div>
              <a href="javascript:;" class="pull-right" style="margin-left:10px" data-aksi="dz_remove"><i class="fa fa-trash-o icon20"></i></a>
              <a href="javascript:;" class="pull-right hide" style="margin-left:10px" data-aksi="dz_undoremove"><i class="fa fa-undo icon20"></i></a>
              <a id="dz_file_`+file.id+`"class="pull-right" href="`+file.url+`" title="`+file.filename+`"><i class="fa fa-eye icon20"></i></a>`);
            $ol.append($li[0].outerHTML);

            $("#dz_file_"+file.id).fancybox({
                helpers : { title : { type : 'over'}}
            });
          })
        }
        $(this).dropzone(_dzconf);
      })
      
      if($(this.selector).find('.my-magnific').length > 0)
        $(this.selector).find('.my-magnific').my_plugins().magnific();
      $(this.selector).find('input[data-type="money"]').each(function(){
        $(this).my_plugins().money();
      })
      $(this.selector).find(".ui-spinner-input").spinner({min:0});
      $(this.selector).find(".ui-spinner-input-persen").spinner({min:0,max:100});
      $(this.selector).find('div.my-summernote').my_plugins().summernote();
      $(this.selector).find(".opt-user-defined").each(function(){
          $(this.selector).find('div.opt-user-defined[name="'+$(this).attr('name')+'"]').my_plugins().optuserdefined();
      })
      return _this;
    }
    this.validate = function(){
      var isHasError=false;
      _this.reset_validate();
      $(this.selector).find('input,select,textarea,.dropzone,.my-summernote').each(function(){
        var _val=null;
        var _msg = null;
        if($(this).attr('required')!='required') return true;
        var $frm_grp=$(this).closest('div.form-group');
        if($(this).hasClass('my-daterange-picker')){
          _val=$(this).datepicker('getDate');
        }else if($(this).hasClass('my-date-picker')){
          _val=$(this).datepicker('getDate');
        }else if($(this).hasClass('dropzone')){
          _val=$(this).closest('.col-inp').find('.dz_nestable_files li.dd-dz-item:not(.dz_removed)').length;
        }else if($(this).hasClass('my-summernote')){
          if($(this).summernote('isEmpty')) _val= null; else _val = 'not null';
        }else if(in_array($(this).attr('type'),['radio'])){
            $(_this.selector).find('input[name="'+$(this).attr('name')+'"]').each(function(){
              if($(this).is(':checked')){
                _val=$(this).val();
                return false;
              }
            })
        }else if(in_array($(this).attr('type'),['checkbox'])){
            var _val = null;
            var _minsel = parseInt($(this).attr('data-selected-min'));
            if(typeof _minsel == 'undefined' || _minsel == null || _minsel=='' || isNaN(_minsel)) _minsel = 1;
            var _nch = 0;
            $(_this.selector).find('input[name="'+$(this).attr('name')+'"]').each(function(){
              if($(this).is(':checked')){
                _nch++;
              }
            })
            if(_minsel<=_nch){ 
                _val=_nch;
            }else{
                _msg = "Pilih paling sedikit "+_minsel;
                _val = null;
            } 
        }else{
          _val=$(this).val();
        }

        if(_val==null || _val==''){
          isHasError=true;
          var frm_lbl='Data '+$frm_grp.find('label.control-label').html()+' wajib diisi.';
          if(_msg!= null) frm_lbl = _msg; 
          $frm_grp.addClass('has-error');
          if($frm_grp.find('.col-inp').find('.hlp-msg').length==0){
            var _lbl = '<label class="hlp-msg">'+frm_lbl+'</label>';
            if($frm_grp.find('.col-inp').find('.text-hint').length>0) $frm_grp.find('.text-hint').before(_lbl);
            else $frm_grp.find('.col-inp').append(_lbl);  
          }
        }else{
          $frm_grp.addClass('has-success');
        }
      })
      
      // HANDLE MAX SELECTED MULTIPLE SELECT2
      $(this.selector).find('select').each(function(){
          var selmax = parseInt($(this).attr('data-selected-max'));
          if(typeof selmax!= 'undefined' && !isNaN(selmax)){
            var vlen = $(this).val();
            if(vlen != null && typeof vlen != 'undefined') vlen = vlen.length; else vlen = null;
            if(vlen != null && vlen>selmax){
                isHasError = true;
                var _msg = 'Pilih tidak lebih dari '+selmax+' pilihan';
                var $frm_grp=$(this).closest('div.form-group');
                $frm_grp.addClass('has-error');
                if($frm_grp.find('.col-inp').find('.hlp-msg').length==0){
                var _lbl = '<label class="hlp-msg">'+_msg+'</label>';
                if($frm_grp.find('.col-inp').find('.text-hint').length>0) $frm_grp.find('.text-hint').before(_lbl);
                else $frm_grp.find('.col-inp').append(_lbl);  
              }
            }
          }
      })
      
      if(isHasError){
        var _target=$(this.selector).find('.has-error:first'); 

        var is_inTab=$(_this.selector).closest('.tab-pane');
        if(is_inTab.hasClass('tab-pane')){
          openTab=is_inTab.attr('id');
          tab=is_inTab.closest('div.panel');
          tab.find('ul.nav li.active').removeClass('active');
          tab.find('div.tab-pane.active').removeClass('active');

          tab.find('a[href="#'+openTab+'"]').closest('li').addClass('active');
          tab.find('div#'+openTab).addClass('active');
        }

        var is_inModal=$(_this.selector).closest('.modal');
        if(is_inModal.hasClass('modal')){
          var scrollIn=is_inModal;
        }else{
          var scrollIn=$('html, body');
        }

        scrollIn.animate({
          scrollTop :$(_target).offset().top
        },1000);
      }
      return !isHasError;
    }
    this.reset = function(){
      _this.reset_validate();
      _this.clear();
      _this.open_first_tab();
    }
    this.open_first_tab = function(){
      var is_inTab=$(this.selector).closest('.tab-pane');
      if(is_inTab.hasClass('tab-pane')){
        tab=is_inTab.closest('div.panel');
        tab.find('ul.nav li.active').removeClass('active');
        tab.find('div.tab-pane.active').removeClass('active');

        tab.find('ul.nav').find('li:first').addClass('active');
        tab.find('.tab-pane:first').addClass('active');
      }
    }
    this.reset_validate = function(){
      $(this.selector).find('.has-error,.has-success').removeClass('has-error').removeClass('has-success');
      $(this.selector).find('.hlp-msg').remove();
    }
    this.clear = function(){
      $(this.selector).find('input,select,textarea,.my-summernote').each(function(){
        var _defval = $(this).attr('def-value');
        if(typeof _defval != 'undefined' && _defval != null && _defval != ''){
          $(this).val(_defval);
        }else if($(this).hasClass('my-date-picker')){
          $(this).datepicker('clearDates');
        }else if($(this).hasClass('my-select2')){
          $(this).val(null).trigger('change');
        }else if($(this).hasClass('my-icheck')){
          $(this).iCheck('uncheck');
        }else if($(this).hasClass('my-summernote')){
          $(this).summernote('reset');
        }else{
          $(this).val('');
        }
      })
    }
    this.serialize = function(){
      var data={};
      $(this.selector).find('input,select,textarea,div.dropzone,div.my-summernote,div.opt-user-defined').each(function(){
        _name=$(this).attr('name');
        if(typeof _name=='undefined') return;
        
        if($(this).attr('data-type')=='money'){
          data[_name]=$(this).attr('data-value');
        }else if($(this).hasClass('my-timerange-picker')){
          _name=$(this).attr('data-attr');
          var $cnt = $(this).closest('div.col-inp');
          data[_name] = {start:null,end:null};
          $cnt.find('input').each(function(i){
            if(i==0) data[_name]['start'] = $(this).val();
            else data[_name]['end'] = $(this).val();  
          })
        }else if($(this).hasClass('my-daterange-picker')){
          _name=$(this).attr('data-attr');
          var $cnt = $(this).closest('div.col-inp');
          data[_name] = {start:null,end:null};
          $cnt.find('.my-date-picker').each(function(i){
            _date=$(this).datepicker('getDate');
            if(_date==null||isNaN(_date.getMonth())) val=null; else val=date_jsToMysql(new Date(_date));
            if(i==0) data[_name]['start'] = val;
            else data[_name]['end'] = val;  
          })
        }else if($(this).hasClass('my-date-picker')){
          _date=$(this).datepicker('getDate');
          if(_date==null||isNaN(_date.getMonth())) val=null; else val=date_jsToMysql(new Date(_date));
          data[_name]=val;
        }else if($(this).hasClass('opt-user-defined')){
            var _val = [];
            $(this).find('.dd-item').each(function(){
                var _check = {};
                $(this).find('div.opt-checks').each(function(){
                    _check[$(this).attr('data-name')] = ['']; 
                    $(this).find('input[type="checkbox"]:checked').each(function(){
                        _check[$(this).attr('data-name')].push($(this).attr('data-value'));
                    })
                })
                _val.push({
                    'id':$(this).attr('data-id'),
                    'option':$(this).find('input[type="text"]').val(),
                    'isAktif':$(this).find('input[type="text"]').attr('data-isaktif'),
                    'tags':$(this).find('input[type="text"]').attr('data-tags'),
                    'checks':_check,
                })
            })
            if(_val.length==0) _val = '';
            data[_name] = _val;    
        }else if($(this).hasClass('select-options-customopt')){
            var _opttags = $(this).find('option[value="'+$(this).val()+'"]').attr('data-tags');
            if(typeof _opttags != 'undefined' && _opttags != null && isStrContain(_opttags,'[opt_others_by_users]')){
                data[_name] = $(this).closest('.col-inp').find('input[type="text"]').val();
            }else data[_name] = $(this).val();
        }else if($(this).hasClass('toggle')){
          if($(this).is(':checked')) data[_name]=$(this).attr('data-true');
          else data[_name]=$(this).attr('data-false');
        }else if($(this).attr('type')=='radio'){
          if($(this).is(':checked')) data[_name]=$(this).val();
        }else if($(this).attr('type')=='checkbox'){
          if($(this).is(':checked')){
            if(typeof data[_name]=='undefined') data[_name]=[$(this).val()];
            else data[_name].push($(this).val())
          };
        }else if($(this).hasClass('dropzone')){
          var $cnt = $(this).closest('.dz_container');
          var file = [];
          $cnt.find('.dz_nestable_files ol li').each(function(){
            if($(this).hasClass('dz_removed')){
              //do nothing
            }else file.push($(this).attr('data-id'));
          })
          if(file.length > 0)
            data[_name] = file;
          else data[_name] = '[NULL]';
        }else if($(this).hasClass('my-summernote')){
          data[_name] = $(this).summernote('code');
        }else{
          data[_name]=$(this).val();
        }

        if($(this).prop('tagName')=='SELECT'){
          var $opt = $(this).find('option:selected');
          if(typeof $opt !== 'undefined' && $opt.length>0){
            $.each($opt.data(),function(i,val){
                if(i.substr(0,9).toLowerCase()=='serialize'){
                  data[i.substr(9)] = val;
                }
            })  
          } 
        }
      })
      return data;
    }
    this.is_empty = function(){
      var _this=this;
      var data=this.serialize();
      var is=true;
      $.each(data,function(i,val){
        if(val==null || val=='' || $(this).attr('type')=='hidden'){
          //do nothing
        }else{
          if($(_this.selector).find('[name="'+i+'"]').attr('type')=='hidden'){
            // do nothing
          }else{
            is=false;
            console.log($(_this).attr('type')+' '+i+' '+val);
            return false;
          }
        }
      })
      return is;
    }
    this.setdata = function(data){
      var _this=this;
      this.clear();
      $.each(data,function(i,val){
        if(val!=null && val!=''){
          var $inp=$(_this.selector).find('[name="'+i+'"]');
          if($inp.hasClass('my-select2')){
                if($inp.prop('multiple')){
                    $inp.val(JSON.parse(val)).trigger('change');
                }else{
                    $inp.val(val).trigger('change');
                }
          }else if($inp.hasClass('my-date-picker')){
            $inp.datepicker('update',new Date(val));
          }else if($inp.hasClass('toggle')){
            if($inp.attr('data-true')==val)
              $inp.bootstrapToggle('on');
            else $inp.bootstrapToggle('off');
          }else if($inp.hasClass('my-icheck')){
            $inp.iCheck('uncheck');
            if(typeof val === 'object'){
              $.each(val, function(_i,_v){
                $(_this.selector).find('[name="'+i+'"][value="'+_v+'"]').iCheck('check');    
              })
            }else
            $(_this.selector).find('[name="'+i+'"][value="'+val+'"]').iCheck('check');
          }else if($inp.hasClass('my-summernote')){
            $inp.summernote('code', val);
          }else{
            $inp.val(val);
          }
        }
      })
    }
    return this;
  }
}(jQuery));

(function ( $ ){
  $.fn.datatable = function(options,colHide=[]){
    var _dt = null;
    var _opt = object_merge(options,{
      buttons : [
        {extend: "print"},
        {extend: "copy"},
        {extend: "csv"},
        {extend: "excel"},
        {extend: "pdf"},
        {extend: "colvisRestore"},
      ],
      colReorder : true,
      columnDefs : [],
      aLengthMenu : [[10, 50, 100, 150, 200, 300, -1], [10, 50, 100, 150, 200, 300, "All"]],
    });
    var _n_toolsBtn = _opt.buttons.length;
    //global var
    var that = this;
    this.colreorderorder = [];
    this.cols = [];
    
    if(typeof options != 'undefined') _opt.basic = options.basic || null;
    
    if(typeof options =='object' && typeof options.url == 'string'){
      _opt.serverSide=true;
      _opt.ajax={
        url:_site_url+options.url,
        data:function(d){
          d.colreorder=that.colreorderorder;
        }
      }
    }
    if(typeof options =='object' && typeof options.order !== 'undefined'){
      _opt.order = options.order;
    }
    if(typeof options =='object' && typeof options.dt_max_colvis !== 'undefined'){
        _opt.dt_max_colvis = options.dt_max_colvis;
    }else _opt.dt_max_colvis = _configs.dt_max_colvis
    
    //Add column filter at footer
    this.addColumnFilter = function(){
      $(this.selector).find('tfoot').html('<tr></tr>');
      var $tfoot=$(this.selector).find('tfoot tr');
      $(this.selector).find('thead th').each(function(){
        var title=$(this).text();
        that.cols.push(title);
        if($(this).attr('data-dt')=='dont-filter')
          $tfoot.append('<th></th>');
        else
          $tfoot.append('<th><input class="form-control" type="text" placeholder="Filter '+title+'" /></th>' );
      })
      that.addColvisBtn();
    }

    this.addColvisBtn = function(){
      var _col='';
      $.each(that.cols,function(i,val){
        if(!in_array(val,colHide))
          _col+='<li><a href="javascript:;" class="dt_btn_tools" data-aksi="'+(i+_n_toolsBtn)+'">'+val+'</a></li>';
      })
      $(that.selector).closest('.panel').find('.panel-heading').find('.panel-title').after(
        '<div class="btn-dt-panel-head btn-group bg-dt-colvis" style="float:right">'+
          '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="octicon octicon-eye"></i>'+
            '<span class="caret ml5"></span>'+
          '</button>'+
          '<ul class="dropdown-menu" role="menu" id="">'+
            _col+
          '</ul>'+
        '</div>');
      that.addToolsBtn();
    }
    //Add tools button
    this.addToolsBtn = function(){
      $(that.selector).closest('.panel').find('.panel-heading').find('.panel-title').after(
      '<div class="btn-dt-panel-head btn-group" style="float:right">'+
        '<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="octicon octicon-tools"></i>'+
          '<span class="caret ml5"></span>'+
        '</button>'+
          '<ul class="dropdown-menu" role="menu">'+
            '<li><a href="javascript:;" class="dt_btn_tools" data-aksi="dt_reload"><i class="glyphicon glyphicon-refresh"></i>&nbsp; Refresh</a></li>'+
            '<li><a href="javascript:;" class="dt_btn_tools" data-aksi="5"><i class="fa fa-columns"></i>&nbsp; Reset Kolom</a></li>'+
            '<li><a href="javascript:;" class="dt_btn_tools" data-aksi="1"><i class="imoon imoon-copy"></i>&nbsp; Copy</a></li>'+
            '<li><a href="javascript:;" class="dt_btn_tools" data-aksi="0"><i class="imoon imoon-print"></i>&nbsp; Print</a></li>'+
            '<li><a href="javascript:;" class="dt_btn_tools" data-aksi="2"><i class="imoon imoon-file"></i>&nbsp; Export CSV</a></li>'+
            '<li><a href="javascript:;" class="dt_btn_tools" data-aksi="3"><i class="imoon imoon-file-excel"></i>&nbsp; Export Excel</a></li>'+
            '<li><a href="javascript:;" class="dt_btn_tools" data-aksi="4"><i class="imoon imoon-file-pdf"></i>&nbsp; Export Pdf</a></li>'+
          '</ul>'+
      '</div>');
      that.handle_btn_color();
    }
    this.handle_btn_color = function(){
      var altColors = ['panel-primary','panel-info','panel-success','panel-warning','panel-danger','panel-alert','panel-system','panel-dark','panel-default','panel-white'];
      var btnColors = 'btn-primary btn-info btn-success btn-warning btn-danger btn-alert btn-system btn-dark btn-default btn-white';
      $.each(altColors,function(i,val){
        if($(that.selector).closest('.panel').hasClass(val)){
          $(that.selector).closest('.panel').find('.btn').removeClass(btnColors).addClass(val.replace('panel','btn'));
          return false;
        }  
      })
      that.get_columnDef();
    }
    //Add colvis buttons
    

    this.get_columnDef = function(){
      $.each(that.cols,function(i,val){
        if(in_array(val,colHide)){
          _opt.columnDefs.push({targets:[i],visible:false});
        }else if(val.toLowerCase()=='aksi'){
          _opt.columnDefs.push({targets:[i],visible:true,orderable:false});
        }else if((i-colHide.length)<_opt.dt_max_colvis) {
          _opt.columnDefs.push({targets:[i],visible:true});
        } else {
          _opt.columnDefs.push({targets:[i],visible:false})
        }; 
        _opt.buttons.push({extend: "columnToggle", columns : i});
      })
      that.init();
    }

    this.colvisstate = function(){
      _dt.columns().flatten().each(function(i){
        var $btn_colvis=$(that.selector).closest('.panel').find('.bg-dt-colvis [data-aksi="'+(i+_n_toolsBtn)+'"]');
        if(_dt.column(i).visible())
          $btn_colvis.removeClass('inactive');
        else $btn_colvis.addClass('inactive');
      })
    },

    this.init = function(){
      _dt=$(this.selector).DataTable(_opt);
      if(_opt.basic) return false;
      
      _dt.columns().flatten().each( function ( colIdx ) {
        $( 'input', _dt.column(colIdx).footer() ).on( 'change', function () {
            _dt.columns().every(function(i){
              _dt.column(that.colreorderorder[i]).search($(this.footer()).find('input').val())
            })
            _dt.draw();
        });
      });
      _dt.on('draw',function(){
        that.colreorderorder=_dt.colReorder.order();
        $(this.selector).find('input.toggle').my_plugins().switch();
      });
      _dt.on('column-visibility.dt',function(e, settings, column, state){
        _dt.columns.adjust().draw();
        $btn_colvis=$(that.selector).closest('.panel').find('.bg-dt-colvis [data-aksi="'+(column+_n_toolsBtn)+'"]');
        if(state)
          $btn_colvis.removeClass('inactive');
        else $btn_colvis.addClass('inactive');
      })
      _dt.on('column-reorder',function(e, settings, details){
        that.colreorderorder=_dt.colReorder.order();
      });
      $(that.selector).closest('.panel').find('.panel-heading').on('click','a.dt_btn_tools',function(){
        var act=$(this).attr('data-aksi');
        if(act=='dt_reload') that.reload();
        _dt.button(act).trigger();
        if(parseInt(act)>=_n_toolsBtn) return false;
      })
      that.colvisstate();
      this._dt  = _dt;

      this.closest('.panel').attr('data-panel-remove','false').attr('data-panel-title','false');
      this.closest('.panel').parent().adminpanel().init();
    }
    
    this.reload = function(){
      _dt.ajax.reload(null,false);
    }

    if(_opt.basic) this.init();
    else this.addColumnFilter();
    

    this.opt = options;
    return this;  
  }
}(jQuery));

(function ( $ ){
  $.fn.page = function(){
    var _this = this;
    this.panel = function(){
        this.find('.panel').attr('data-panel-remove','false').attr('data-panel-title','false');
        this.parent().adminpanel().init();
    }
    this.serialize = function(){
      var data = {};
      $(this.selector).find('form').each(function(){
        var _id = $(this).attr('id');
        data[_id] = $('form#'+_id).form().serialize();
      })
      return data;
    }
    this.validate = function(){
      var res = true;
      $(this.selector).find('form').each(function(){
        var _id = $(this).attr('id');
        var _res = $('form#'+_id).form().validate();
        if(res) res = _res;
      })
      return res;
    }
    this.block = function(){
      if($(this.selector).hasClass('modal'))
        App.blockUI({target: $(this.selector).find('.modal-body'),animate: true}); 
      else
        App.blockUI({target: $(this.selector),animate: true});
    }
    this.unblock = function(){
      if($(this.selector).hasClass('modal'))
        App.unblockUI($(this.selector).find('.modal-body')); 
      else
        App.unblockUI($(this.selector));
    }
    this.disable_btn = function(btns='all'){
      if(btns=='all') btns=['close','save'];
      if(my_libs.in_array('close',btns)){
        $(this.selector).find('button.btn[data-dismiss="modal"]').addClass('disabled');
        $(this.selector).find('button.close[data-dismiss="modal"]').hide();
      }
      if(my_libs.in_array('save',btns)){
        $(this.selector).find('button.btn[data-aksi="simpan"]').addClass('disabled');
      }
    }
    this.enable_btn = function(btns='all'){
      if(btns=='all') btns=['close','save'];
      if(my_libs.in_array('close',btns)){
        $(this.selector).find('button.btn[data-dismiss="modal"]').removeClass('disabled');
        $(this.selector).find('button.close[data-dismiss="modal"]').show();
      } 
      if(my_libs.in_array('save',btns)){
        $(this.selector).find('button.btn[data-aksi="simpan"]').removeClass('disabled');
      }
    }
    this.mdl_open = function(options={title:'Blank Modal',clearbody:true}){
      $(this.selector).find('.modal-title').html(options.title);
      if(options.clearbody)
          $(this.selector).find('.modal-body').html('');
      $(this.selector).modal('show');
      _this.disable_btn();
      _this.block();
    }
    this.mdl_hide = function(){
      $(this.selector).modal('hide');
    }
    this.notify = function(type,title,msg){
      var Stacks = {
      stack_top_right: {
        "dir1": "down",
        "dir2": "left",
        "push": "top",
        "spacing1": 10,
        "spacing2": 10
      },
      stack_top_left: {
        "dir1": "down",
        "dir2": "right",
        "push": "top",
        "spacing1": 10,
        "spacing2": 10
      },
      stack_bottom_left: {
        "dir1": "right",
        "dir2": "up",
        "push": "top",
        "spacing1": 10,
        "spacing2": 10
      },
      stack_bottom_right: {
        "dir1": "left",
        "dir2": "up",
        "push": "top",
        "spacing1": 10,
        "spacing2": 10
      },
      stack_bar_top: {
        "dir1": "down",
        "dir2": "right",
        "push": "top",
        "spacing1": 0,
        "spacing2": 0
      },
      stack_bar_bottom: {
        "dir1": "up",
        "dir2": "right",
        "spacing1": 0,
        "spacing2": 0
      },
      stack_context: {
        "dir1": "down",
        "dir2": "left",
        "context": $("#stack-context")
      },
    }
    if(type=='error') var _hide = false; else var _hide = true;
      new PNotify({
        title: title,
        text: msg,
        shadow: true,
        opacity: 1,
        type: type,
        stack: Stacks['stack_top_right'],
        width: '290px',
        delay: 3000,
        hide : _hide
      });
    }
    return this;
  }
}(jQuery));

function callback_triggerResize(){
  $('body').find('table.dataTable').each(function(){
    $(this).css('width','100%');
    $dt=$(this).DataTable();
    setTimeout(function() {
      $dt.columns.adjust().draw();
    }, 300)
    
  })
}
function callback_panelControlCreated(){

}
var my_libs = {
    collect_form : function($form){
        var data={};
        $form.find('input,select,textarea').each(function(){
            var name=$(this).attr('name');
            var id=$(this).attr('id');
            if(name==null) var par=id; else var par=name;
            
            var val=null;
            if($(this).hasClass('date-picker')){
                var date=new Date($(this).datepicker('getDate'));
                if(isNaN(date.getMonth())) val=null;
                else val=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
            }else val=$(this).val();
            
            data[par]=val;
        })
        return data;
    },
    collect_table : function($table){
        var data={};
        $table.find('tbody tr').each(function(){
            var _data={};
            $(this).find('input,select,textarea').each(function(){
                if($(this).hasClass('date-picker')){
                    var date=new Date($(this).datepicker('getDate'));
                    if(isNaN(date.getMonth())) val=null;
                    else val=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
                    _data[$(this).attr('name')]=val;
                }else
                if(!$(this).hasClass('uncollectible'))
                    _data[$(this).attr('name')]=$(this).val();
            })
            data[$(this).attr('data-id')]=_data;
        })
        return data;
    },
    in_array : function(needle, haystack){
        var length = haystack.length;
        for(var i = 0; i < length; i++) {
            if(typeof haystack[i] == 'object') {
                if(arrayCompare(haystack[i], needle)) return true;
            } else {
                if(haystack[i] == needle) return true;
            }
        }
        return false;
    }
}
var App = function() {
    return {
        blockUI: function(options) {
            options = $.extend(true, {}, options);
            var html = '';
            if (options.animate) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '">' + '<div class="block-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>' + '</div>';
            } else if (options.iconOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img src="' + this.getGlobalImgPath() + 'loading-spinner-grey.gif" align=""></div>';
            } else if (options.textOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><span>&nbsp;&nbsp;' + (options.message ? options.message : 'LOADING...') + '</span></div>';
            } else {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img src="' + this.getGlobalImgPath() + 'loading-spinner-grey.gif" align=""><span>&nbsp;&nbsp;' + (options.message ? options.message : 'LOADING...') + '</span></div>';
            }
        
            if (options.target) { // element blocking
                var el = $(options.target);
                if (el.height() <= ($(window).height())) {
                    options.cenrerY = true;
                }
                el.block({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    centerY: options.cenrerY !== undefined ? options.cenrerY : false,
                    css: {
                        top: '10%',
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            } else { // page blocking
                $.blockUI({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            }
        },
        
        // wrApper function to  un-block element(finish loading)
        unblockUI: function(target) {
            if (target) {
                $(target).unblock({
                    onUnblock: function() {
                        $(target).css('position', '');
                        $(target).css('zoom', '');
                    }
                });
            } else {
                $.unblockUI();
            }
        },
    }
}();


// =============== HANDLE INPUT TYPE MONEY ====================================
$('body').on('focus','input[data-type="money"]',function(){
  var raw = $(this).attr('data-value');
  if(typeof raw == 'undefined' || raw ==null){
    $(this).val('');
  }else $(this).val(raw);
})
$('body').on('blur','input[data-type="money"]',function(){
  $(this).attr('data-value',$(this).val());
  $(this).val(number_to_currency($(this).val()));
})
$('body').on('keypress','input[data-type="money"]',function(event){
  return isNumberKey(event);
})
//=============================================================================
// =============== HANDLE DROPZONE CUSTOM ACTOIN ==============================
$('body').on('click','a[data-aksi="dz_remove"]',function(){
  var $li = $(this).closest('li');
  $li.addClass('dz_removed');
  $li.find('a[data-aksi="dz_undoremove"]').removeClass('hide');
  $(this).addClass('hide');
})
$('body').on('click','a[data-aksi="dz_undoremove"]',function(){
  var $li = $(this).closest('li');
  $li.removeClass('dz_removed');
  $li.find('a[data-aksi="dz_remove"]').removeClass('hide');
  $(this).addClass('hide');
})
//=============================================================================
// =============== HANDLE SELECT WITH CUSTOM OPTION ===========================
$('body').on('change','select.select-options-customopt',function(){
    var _opttags = $(this).find('option[value="'+$(this).val()+'"]').attr('data-tags');
    $(this).closest('.col-inp').find('input').remove();
    
    // HANDLE LABEL
    var label = $(this).closest('.form-group').find('.control-label').html();
    label = label.replace('<span class="label-required">*</span>','');
    
    // HANDLE REQUIRED ATTRIBUTE
    var req = "";
    var isReq = $(this).attr('required');
    if(typeof isReq != 'undefined' && isReq!= null && isReq == 'required') req = ' required="required"';
        
    if(typeof _opttags != 'undefined' && _opttags != null && isStrContain(_opttags,'[opt_others_by_users]')){
        $(this).closest('.col-inp').append('<input type="text" class="form-control mt5" placeholder="Tuliskan '+label+'" '+req+'>');
    }
})
//=============================================================================
// ==================== HANDLE MY COLLAPSIBLE PANEL ===========================
$('body').on('click','a.my-collapse',function(){
    $(this).closest('.panel').find('.panel-body').hide(400);
    $(this).removeClass('my-collapse').addClass('my-expand').find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
})
$('body').on('click','a.my-expand',function(){
    $(this).closest('.panel').find('.panel-body').show(400);
    $(this).removeClass('my-expand').addClass('my-collapse').find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
})
//=============================================================================