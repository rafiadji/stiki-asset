var dropdownFilter = {

  // Declare any variables we will need as properties of the object
  $filters: null,
  $reset: null,
  groups: [],
  outputArray: [],
  outputString: '',

  // The "init" method will run on document ready and cache any jQuery objects we will need.
  init: function (filter, reset, container) {
    var self = this; // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent "dropdownFilter" object so that we can share methods and properties between all parts of the object.

    self.$filters = $(filter);
    self.$reset = $(reset);
    self.$container = $(container);

    self.$filters.find('fieldset').each(function () {
      self.groups.push({
        $dropdown: $(this).find('select'),
        active: ''
      });
    });

    self.bindHandlers();
  },

  // The "bindHandlers" method will listen for whenever a select is changed. 
  bindHandlers: function () {
    var self = this;

    // Handle select change    
    self.$filters.on('change', 'select', function (e) {
      e.preventDefault();

      self.parseFilters();
    });

    // Handle reset click
    self.$reset.on('click', function (e) {
      e.preventDefault();

      self.$filters.find('select').val('').trigger('click');
      self.$filters.find('ul.multiselect-container li.active').removeClass('active').find('input').prop('checked',false);
      self.$filters.find('ul.multiselect-container li:first').addClass('active').find('input').prop('checked',true);
      self.$filters.find('.multiselect.dropdown-toggle').html('Semua&nbsp;<b class="caret"></b>');

      self.parseFilters();
    });
  },

  // The parseFilters method pulls the value of each active select option
  parseFilters: function () {
    var self = this;

    // loop through each filter group and grap the value from each one.
    for (var i = 0, group; group = self.groups[i]; i++) {
      group.active = group.$dropdown.val();
    }

    self.concatenate();
  },

  // The "concatenate" method will crawl through each group, concatenating filters as desired:
  concatenate: function () {
    var self = this;

    self.outputString = ''; // Reset output string

    for (var i = 0, group; group = self.groups[i]; i++) {
      self.outputString += group.active;
    }

    // If the output string is empty, show all rather than none:
    !self.outputString.length && (self.outputString = 'all');

    //console.log(self.outputString); 
    // ^ we can check the console here to take a look at the filter string that is produced

    // Send the output string to MixItUp via the 'filter' method:
    if (self.$container.mixItUp('isLoaded')) {
      self.$container.mixItUp('filter', self.outputString);
    }
  },

  magnificPopup: function ($container) {
   $container.find('a[data-type="image"]>img').magnificPopup({
      type: 'image',
      callbacks: {
        beforeOpen: function (e) {
          // we add a class to body to indicate overlay is active
          // We can use this to alter any elements such as form popups
          // that need a higher z-index to properly display in overlays
          $('body').addClass('mfp-bg-open');

          // Set Magnific Animation
          this.st.mainClass = 'mfp-zoomIn';

          // Inform content container there is an animation
          this.contentContainer.addClass('mfp-with-anim');
        },
        afterClose: function (e) {

          setTimeout(function () {
            $('body').removeClass('mfp-bg-open');
            $(window).trigger('resize');
          }, 1000)

        },
        elementParse: function (item) {
          // Function will fire for each target element
          // "item.el" is a target DOM element (if present)
          // "item.src" is a source that you may modify
          item.src = item.el.attr('src');
        },
      },
      overflowY: 'scroll',
      removalDelay: 200, //delay removal by X to allow out-animation
      prependTo: $('#content_wrapper')
    });
  }
};

$('body').on('click','a.stp-upload-file',function(){
  var _this=$(this);
  var _modal=$('#magnific_md');
  var _href=$(this).attr('data-href');
  _modal.find('.modal-dialog').removeClass('modal-lg').addClass('modal-md');
  _modal.find('.modal-title').html($(this).find('img').attr('title')+' ('+$(this).attr('data-syaratname')+')');
  _modal.find('.modal-body').html('<div class="row"><div class="stp-upload-ver-prev">'+$(this).closest('div.mix').html()+'</div><div>');
  _modal.find('img.img-responsive').closest('div.panel').css('max-width','50%').css('margin-left','auto').css('margin-right','auto');
  if(typeof _href!='undefined' && _href.length>0 && _href!='javascript:;')
    _modal.find('.stp-upload-ver-prev>.panel').append('<a href="'+_href+'" target="_blank" class="btn btn-info btn-block"><i class="octicon octicon-cloud-download"></i>&nbsp;Unduh Berkas</a>');
  if($(this).attr('data-type')=='image')
    dropdownFilter.magnificPopup(_modal);
  else _modal.find('a.stp-upload-file').attr('href',$(this).attr('data-href'));
  
  _modal.find('.modal-body .stp-upload-file').removeClass('stp-upload-file');
  var _color=$(this).closest('div.panel').find('.ribbon').attr('data-color')
  var _vernote=$(this).attr('data-note');
  if(_vernote.length>0) _modal.find('.stp-upload-ver-prev').append('<div class="alert alert-micro alert-border-left alert-'+_color+'"><p>'+urldecode(_vernote)+'</p></div>');
  
  if(_color=='danger'){
    _modal.find('.modal-dialog').removeClass('modal-md').addClass('modal-lg');
    _modal.find('.stp-upload-ver-prev').addClass('col-lg-6');
    _modal.find('.modal-body>.row').append('<div class="stp-upload-rev col-lg-6" style="min-height:100px"></div>');
    var $panel_rev=_modal.find('.stp-upload-rev').my_plugins().page();
    setTimeout(function(){ 
      $panel_rev.block();
      var idstp=_this.closest('.stp-upload-done').attr('data-stp');
      $.post(_site_url + '/adm_stp_permohonan/upload', {id: idstp,syarat:_this.attr('data-syarat')}, function (respon) {
        $panel_rev.append(respon);
        $panel_rev.find('#accordion-upload .accordion-toggle:first').trigger('click');
        $panel_rev.find('.wajib-upload').remove();
        $panel_rev.find('[data-aksi="submit-berkas"]').remove();
        $panel_rev.unblock();
      }).fail(function(){
        $panel_rev.unblock();
        $panel_rev.notify('error', _page_info.title, _msg.unknow_error);
      })
    }, 400);
  }  
  _modal.modal('show');
})
$('#magnific_md').on('hide.bs.modal', function (e) {
  var _modal=$('#magnific_md');
  var idstp=_modal.find('input[name="id_permohonan"]').val();
  if(_modal.find('#accordion-upload').length>0){
    usr_stp.timelines[idstp].reload();
  }
})
