$.fn.a52form = function(config) 
{
	return this.each(function()
	{
		var e = $(this);
		var id;
		var message;
		var selector;
		var msg_container = '.a52-form-msg';
		var config = $.extend({
			isAjax: e.attr('ajax'),
			action: e.attr('action'),
			method: e.attr('method'),
			title: e.attr('title'),
			validate: e.is('[data-a52-validate]'),
		}, config);
		
		// method
		e.initialize = function()
		{
			e.id = Math.floor((Math.random() * 10000) + 1);
			e.selector = '[a52-form-id="'+e.id+'"]';
			e.attr('a52-form-id', e.id);
			e = $(e.selector);
			
			// load datepicker
			if($().datepicker) $('[datepicker]', e).datepicker();
			
			// load toggle
			if($().bootstrapToggle) 
			{
				$('[toggle]', e).each(function()
				{
					if($(this).data('value') == $(this).data('true'))
						$(this).bootstrapToggle('on').trigger('change');
					else
						$(this).bootstrapToggle('off').trigger('change');
				})
			}
		}
		e.initialize();
		
		e.showNotify = function(type, message, title)
		{
			if (typeof PNotify !== "undefined") 
			{ 
				new PNotify({
					title: config.title,
					text: message,
					shadow: true,
					opacity: 1,
					type: type,
					stack: {"dir1": "down", "dir2": "left", "push": "top", "spacing1": 10, "spacing2": 10},
					width: '290px',
					delay: 3000
				});
			}
			else e.showMessage(type, message)
		}
		
		e.showMessage = function(type, message)
		{
			message = '<div class="alert alert-dismissable alert-'+type+'">'+message+'</div>';
			if($(msg_container, e).length) 
				$(msg_container, e).html(message);
			else
			{
				message = '<div class="'+msg_container.replace('.','')+'">'+message+'</div>';
				e.prepend(message);
			}
			if(e.parents('.modal').length) e.parents('.modal').animate({ scrollTop: $(msg_container).offset().top}, 500);
			else $('html, body').animate({ scrollTop: $(msg_container).offset().top}, 500);
			e.find('.has-error').first().find('[validate]').first().focus()
		}
		
		e.hideMessage = function()
		{
			$(msg_container, e).remove();
		}

		e.validate = function(){
			var error = [];
		
			// validate each fields		
			$('[validate]', e).each(function()
			{
				var value = $(this).val().trim();
				var is_error = false;
				var attrs = $(this).attr('validate').split(';');
				
				var parent_container = '.form-group';
				var label = $(this).attr('label');
				if (label == undefined) label = $(this).parents(parent_container).find('label').first().html();
				if (label == undefined) label = $(this).parents('tr').find('th:first-child').first().html();
				if (label == undefined) label = $(this).attr('name');
				label = '<b>' + label + '</b>';
				
				// reset 
				$(this).parent().removeClass('has-error');
				
				for(var i=0; i<attrs.length; i++)
				{
					var attr = attrs[i];
					if (is_error) break;
					if (attr == '') continue;
					
					switch(attr) {
						
						// required
						case 'required':
							if (value == '' || value == undefined || value == null) is_error = label + ' tidak boleh kosong.';
							break;
						
						// email
						case 'email':
							var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
							if (!re.test(value)) is_error = label + ' harus berisi alamat email yang valid.';
							break;
						
						// number
						case 'number':
							if (isNaN(value)) is_error = label + ' harus berisi angka.';
							break;

						// date-ind
						case 'date-ind':
							if (!value.match(/^\d{2}\/\d{2}\/\d{4}$/)) is_error = label + ' harus berformat dd/mm/yyyy, contoh 21/03/2018.';
							break;
							
							// code
						case 'code':
							if (!value.match(/^[a-zA-Z0-9\_\-\.]{3,50}$/)) is_error = label + ' hanya boleh berisi angka, huruf, - (minus), _ (garis bawah), .(titik)';
							break;
							
						// exact:number
						case (attr.match(/^exact\:.*$/) || {}).input :
							var param = attr.match(/^exact\:(.*)$/)[1];
							if (value.length != param ) is_error = label + ' harus '+param+' karakter.';
							break;
						
						// min:number
						case (attr.match(/^min\:.*$/) || {}).input  :
							var param = attr.match(/^min\:(.*)$/)[1];
							if (value.length < param ) is_error = label + ' tidak boleh kurang dari '+param+' karakter.';
							break;
						
						// max:number
						case (attr.match(/^max\:.*$/) || {}).input  :
							var param = attr.match(/^max\:(.*)$/)[1];
							if (value.length > param ) is_error = label + ' tidak boleh lebih dari '+param+' karakter.';
							break;
						
						// match:selector
						case (attr.match(/^match\:.*$/) || {}).input  :
							var param = attr.match(/^match\:(.*?)$/)[1];
							if($(param).length && value != $(param).val())
							{
								var label_match = $(param).attr('label');
								if (label_match == undefined) label_match = $(param).parents(parent_container).find('label').first().html();
								if (label_match == undefined) label_match = $(param).parents('tr').find('th:first-child').first().html();
								if (label_match == undefined) label_match = $(param).attr('name');
								label_match = '<b>' + label_match + '</b>';
								is_error = label + ' tidak boleh berbeda dari '+label_match;
							}
							break;
						
						default:
					}
					
					if (is_error) 
					{
						$(this).parents(parent_container).addClass('has-error');
						error.push(is_error);
						break;
					} else $(this).parents(parent_container).removeClass('has-error');
				}
				
			});	
		
			// if error not found
			if (error.length < 1) 
			{
				e.hideMessage();
				return true;
			}
			
			// generate error message
			var error_html = '<ul>';
			for (var i=0; i < error.length; i++) error_html += '<li>'+error[i]+'</li>';
			error_html += '</ul>'
			error_html = 'Anda tidak bisa melanjutkan ke proses selanjutnya dikarenakan hal berikut : <br><br>'+error_html;
			e.showMessage('danger',error_html);
			e.find('.has-error').first().find('[validate]').first().focus();
			return false;
		}

		// event method
		$('body').on('submit', e.selector ,function(submit_e, submit_conf){
			var submit_conf = $.extend({
				success: false,
				error: false,
			}, submit_conf);
			
			// validate form
			if(config.validate && !e.validate()) 
			{
				if(typeof submit_conf.error === 'function') submit_conf.error({success:false, message: 'Gagal validasi'});
				return false;
			}
			
			// if ajax
			if(config.isAjax)
			{
				var data = e.serializeArray();
				var new_data = {};
				
				// filter datepicker
				if($().datepicker) $('[datepicker]', e).each(function()
				{
					name = $(this).attr('name');
					val = $(this).datepicker('getDate');
					val = new Date(val);
					new_data[name] = [val.getFullYear(), val.getMonth()+1, val.getDate()].join('-');
				})
				
				// filter toggle
				if($().bootstrapToggle) $('[toggle]', e).each(function()
				{
					name = $(this).attr('name');
					val =  $(this).is(':checked') ? $(this).data('true') : $(this).data('false');
					new_data[name] = val;
				})
				
				// apply filter
				for(var i=0; i<data.length; i++)
				{
					curr = data[i];
					if(new_data.hasOwnProperty(curr.name)) 
					{
						data[i].value = new_data[curr.name];
						delete new_data[curr.name];
					}
				}
				$.each(new_data, function(key, val){
					data.push({'name':key, 'value':val});
					delete new_data[key];
				})
				
				$('[type=submit], submit', e).attr('disabled', 'disabled');
				$.ajax({
					url : config.action,
					method : config.method,
					data : data,
					dataType : 'JSON',
					success: function (data)
					{
						if (data.success)
						{
							e.showNotify('success', data.message || 'Data berhasil disimpan');
							if(typeof submit_conf.success === 'function') submit_conf.success(data);
						}
						else 
						{
							e.showNotify('danger', data.message || 'Data gagal disimpan !, Harap hubungi operator.');
							if(typeof submit_conf.error === 'function') submit_conf.error(data);
						}
						$('[type=submit], submit', e).removeAttr('disabled');
					},
					error : function (error){
						e.showNotify('danger', 'Terjadi kesalahan : Error 500 !, Harap hubungi operator.');
						$('[type=submit], submit', e).removeAttr('disabled');
						if(typeof submit_conf.error === 'function') submit_conf.error(error);
					}
				});
				return false;
			}

			return true;
		})

		
		return this;
	})
}

var a52form = $('.a52-form').a52form();