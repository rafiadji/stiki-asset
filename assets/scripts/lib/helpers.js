function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which:event.keyCode
    if(charCode >31 && (charCode<48 || charCode>57)) return false; return true;
};
function urldecode(url){
    if(typeof url == 'undefined') return false;
    return decodeURIComponent(url.replace(/\+/g, ' '));
}
function urlencode(url){
    return encodeURIComponent(url);
}
function date_jsToMysql(_date){
	return _date.getFullYear()+'-'+(_date.getMonth()+1)+'-'+_date.getDate();
}
function date_jsFromMysql(stringdate){
    var _date = new Date();
    _str = stringdate.split('-');
    _date.setFullYear(_str[0]);
    _date.setMonth(parseInt(_str[1])-1);
    _date.setDate(_str[2]);
    return _date;
}
function object_merge(obj1,obj2){
    var result={};
    $.extend(result, obj1, obj2);
    return result;
}
function in_array(needle, haystack){
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(typeof haystack[i] == 'object') {
            if(arrayCompare(haystack[i], needle)) return true;
        } else {
            if(haystack[i] == needle) return true;
        }
    }
    return false;
}
function is_validEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
function is_validJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
function uniqid(){
    return Math.floor(new Date().getTime()/1000).toString(16)+("000000"+performance.now()%1*1e6).toString(16).slice(-5);
}
function number_to_currency(number,symbol='Rp. '){
    if(number==null) return "Rp. -,00";
    return symbol+number.toString().replace(/\B(?=(\d{3})+(?!\d))/g,".")+",00";
}
function redirect(url){
  window.location.href = url;
}
function htmlGetAllAttr($elm){
  var result = {};
  $.each($elm[0].attributes, function(){
    if(this.specified){
      result[this.name] = this.value;
    }
  })
  return result;
}
function isStrContain(str,chr){
    return str.includes(chr);
}
