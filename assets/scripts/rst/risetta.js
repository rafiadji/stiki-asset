var template = {
    sel2_ajax_conf: {
        ajax: {
            url: _page_info.url + '/get_optUser',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }

                return query;
            }
        },
        placeholder: 'Cari person',
        minimumInputLength: 3,

    },
}
var rst_ta = {
    dt: null,
    frm: null,
    frm_sub: null,
    frm_jns : null,
    frm_mic: [],
    mdl_inp: $('#mdl_inp').my_plugins().page(),
    mdl_prf: $('#mdl_lht').my_plugins().page(),
    open_profile: function (par) {
        var _this = this;
        this.mdl_prf.mdl_open({ title: par.nama, clearbody: false });
        $.post(_page_info.url + '/get_riset', { id: par.id }, function (respon) {
            _this.mdl_prf.find('.modal-body').html(respon);
            _this.mdl_prf.enable_btn();
        }).fail(function () {
            _this.mdl_prf.notify('error', _page_info.title, _msg.unknow_error);
        })
    },
    open_icheck: function (par) {
        var _this = this;
        $('.vw').html('');
        $.post(_page_info.url + '/icheck',par, function (respon) {
            $('.vw').html(respon);
        })
    },
    open_summary: function () {
        var _this = this;
        $.post(_page_info.url + '/summary', function (respon) {
            $('#dt_panel .panel-body').html(respon);
        })
    },
    open_dialog_input: function (mode, callback = null, par = null) {
        var _this = this;
        this.frm_mic = [];
        this.mdl_inp.mdl_open({ title: mode + ' ' + _page_info.title, clearbody: false });
        par = this.frm_jns.serialize();
        
        $.post(_page_info.url + '/get_formByJenis', par, function (respon) {
            _this.mdl_inp.find('.modal-body').html(respon);
            _this.frm = $('form[id^="refdef-"]').my_plugins().form();

            _this.mdl_inp.enable_btn();
        }).fail(function () {
            _this.mdl_inp.notify('error', _page_info.title, _msg.unknow_error);
        })
        _this.mdl_inp.unblock();
        _this.mdl_inp.enable_btn();
    },
    simpan: function () {
        var _this = this;
        if (this.frm.validate()) {
            this.mdl_inp.block();
            this.mdl_inp.disable_btn();
            var data = { prop: this.frm.serialize() }

            $.post(_page_info.url + '/set_simpan_topic', data, function (respon) {
                if (respon.isOk) {
                    _this.mdl_inp.notify('success', _page_info.title, 'Riset baru berhasil ditambahkan');
                    _this.mdl_inp.mdl_hide();
                    _this.open_summary();
                } else {
                    _this.mdl_inp.notify('error', _page_info.title, respon.msg);
                }
                _this.mdl_inp.unblock();
                _this.mdl_inp.enable_btn();
            }, 'json').fail(function () {
                _this.mdl_inp.notify('error', _page_info.title, _msg.unknow_error);
                _this.mdl_inp.unblock();
                _this.mdl_inp.enable_btn();
            })
        }
    },
    hapus: function (id) {
        var _this = this;
        $.post(_page_info.url + '/set_hapus', { id: id }, function (respon) {
            if (respon.isOk) {
                _this.mdl_inp.notify('success', _page_info.title, 'Riset berhasil dihapus.');
                _this.dt.reload();
            } else {
                _this.mdl_inp.notify('error', _page_info.title, respon.msg);
            }
        }, 'json').fail(function () {
            _this.mdl_inp.notify('error', _page_info.title, _msg.unknow_error);
        })
    },
}
$(document).ready(function () {
    rst_ta.frm_jns = $('form#refta-optjenis-rst_tugasakhir').my_plugins().form();
    rst_ta.open_icheck(rst_ta.frm_jns.serialize());
})

rst_ta.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
    rst_ta.simpan();
})
$('body').on('click','button[data-aksi="proposal"]',function(){
    rst_ta.open_dialog_input('Porposal', 'proposal');
})
$('body').on('change','form#refta-optjenis-rst_tugasakhir select[name="jenis_riset"]',function(){
    rst_ta.open_icheck(rst_ta.frm_jns.serialize());
})