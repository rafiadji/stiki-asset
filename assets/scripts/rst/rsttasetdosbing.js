var setdosbing = {
    dt : null,
    mdl_lht : $('#mdl_lht').my_plugins().page(),
    mdl_inp : $('#mdl_inpdosbing').my_plugins().page(),
    frm_res : null,
    lihat_rst : function(par){
        var _this=this;
        this.mdl_lht.mdl_open({title:par.nama,clearbody:false});
        $.post(_page_info.url+'/get_riset',{id:par.id},function(respon){
            _this.mdl_lht.find('.modal-body').html(respon);
            _this.mdl_lht.enable_btn();
        }).fail(function(){
            _this.mdl_lht.enable_btn();
            _this.mdl_lht.notify('error',_page_info.title,_msg.unknow_error);
        })
    },
    set_dosbing : function(par){
        var _this=this;
        this.mdl_inp.mdl_open({title:par.nama,clearbody:false});
        $.post(_page_info.url+'/get_formrespon',{id:par.id},function(respon){
            _this.mdl_inp.find('.modal-body').html(respon);
            _this.frm_res = $('form#rstsubsk').my_plugins().form();
            
            var $tb = _this.mdl_inp.find('table.tbrstkon');
            $tb.find('tbody tr').my_plugins().form();
            $tb.find('tbody tr').find('select[name="user_username"]').my_plugins().select2(template.sel2_ajax_conf);
            _this.mdl_inp.enable_btn();
        }).fail(function(){
            _this.mdl_lht.enable_btn();
            _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
        })
    },
    respon_simpan : function(){
        var _this=this;
        
        if(_this.mdl_inp.validate()){
            var data = _this.mdl_inp.serialize();
            data = object_merge(data,rst_riset.tb_str.serialize());
            _this.mdl_inp.block();
            _this.mdl_inp.disable_btn();
            $.post(_page_info.url+'/set_simpankontributor',data,function(respon){
                if(respon.isOk){
                    _this.mdl_inp.notify('success',_page_info.title,'Berhasil menyimpan '+_page_info.title);
                    _this.mdl_inp.mdl_hide();
                    _this.dt.reload();
                }else{
                    _this.mdl_inp.notify('error',_page_info.title,respon.msg);
                }
                _this.mdl_inp.enable_btn();
                _this.mdl_inp.unblock();
            },'json').fail(function(){
                _this.mdl_inp.enable_btn();
                _this.mdl_inp.unblock();
                _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
            })
        
        }
    },
}
$(document).ready(function(){
    //setdosbing.dt=$('#main_dt').my_plugins().datatable({url:'/'+_page_info.classname+'/getdata'},['id']);
})
$('body').on('click','a[data-aksi="rst-lihat"]',function(){
    var par = JSON.parse(urldecode($(this).attr('data-par')));
    setdosbing.lihat_rst(par);
})
$('body').on('click','a[data-aksi="rstta-setdosbing"]',function(){
    var par = JSON.parse(urldecode($(this).attr('data-par')));
    setdosbing.set_dosbing(par);
})
setdosbing.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
    setdosbing.respon_simpan();
})