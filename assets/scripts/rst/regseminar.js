var regsmnr = {
    tb_history : null,
    dt: null,
    _frm_sel : 'form#rst_regseminar-selectkbsi',
    _frm_smnr : 'form#rst_regseminar-regseminar',
    frm_sel : null,
    frm_smnr: null,
    panel_frm : $('#panel-formseminar').my_plugins().page(),
    mdl_inp: $('#mdl_inp').my_plugins().page(),
    mdl_prf: $('#mdl_lht').my_plugins().page(),
    open_detail: function (mode, callback = null, par = null) {
        var _this = this;
        this.frm_mic = [];
        this.mdl_prf.mdl_open({ title: mode + ' ' + _page_info.title, clearbody: false });

        $.post(_page_info.url + '/detail_pengajuan', par, function (respon) {
            _this.mdl_prf.find('.modal-body').html(respon);
            _this.mdl_prf.enable_btn();
        }).fail(function () {
            _this.mdl_prf.notify('error', _page_info.title, _msg.unknow_error);
        })
        _this.mdl_prf.unblock();
        _this.mdl_prf.enable_btn();
    },
    get_form_seminar(){
        var _this = this;
        par = this.frm_sel.serialize();
        
        _this.panel_frm.show(400);
        setTimeout(function(){
            _this.panel_frm.block();
            $.post(_page_info.url + '/form_seminar', par, function (respon) {
                _this.panel_frm.find('.panel-body').html(respon);
                _this.frm_smnr = $(_this._frm_smnr).my_plugins().form();
                _this.panel_frm.unblock();
            }).fail(function () {
                _this.panel_frm.notify('error', _page_info.title, _msg.unknow_error);
            })
        },400)
        
        
    },
    get_lihatreg(id, target = null){
        var _this = this;
        
        var data = {id:id};

        $.post(_page_info.url+'/detail_pengajuan',data,function(respon){
            if(target!=null) $(target).html(respon);
        }).fail(function(){
            _this.panel_frm.notify('error',_page_info.title,_msg.unknow_error);
        })
    },
    set_submitreg(ins){
        var _this = this;
        if(_this.frm_smnr.validate()){
            var data = object_merge(_this.frm_sel.serialize(),_this.frm_smnr.serialize());
            
            $.post(_page_info.url+'/set_simpankontribusi',data,function(respon){
                if(respon.isOk){
                    _this.panel_frm.notify('success',_page_info.title,'Berhasil menyimpan '+_page_info.title);
                    _this.get_lihatreg(respon.data.id,$(ins).closest('.tab-pane'));
                    _this.dt.reload();
                }else{
                    _this.panel_frm.notify('error',_page_info.title,respon.msg);
                }
            },'json').fail(function(){
                _this.panel_frm.notify('error',_page_info.title,_msg.unknow_error);
            })
        }
    },
    init : function(){
        this.frm_sel = $(this._frm_sel).my_plugins().form();
        if($(this._frm_sel).find('input[name="kontribusi"]').length>0)
            this.get_form_seminar();
        var session = JSON.parse(atob(atob(_page_data.session)));
        this.dt=$('#main_dt').my_plugins().datatable({url:'/'+_page_info.classname+'/getdata/'+session.id},['id']);
    },
}
$(document).ready(function () {
    regsmnr.init();
    regsmnr.tb_history = $('#tb_history').my_plugins().datatable();
})
$('body').on('change',regsmnr._frm_sel+' select[name="jenis_kontribusi"]',function(){
    regsmnr.get_form_seminar();
})
$('button[data-aksi="det_pengajuan"]').click(function () {
    regsmnr.open_detail('Detail');
})
$('body').on('click','a[data-aksi="submit_seminar"]',function(){
    regsmnr.set_submitreg($(this));
})