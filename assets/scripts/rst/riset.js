var template = {
    sel2_ajax_conf : {
        ajax : {
            url: _page_info.url+'/get_optUser',
            dataType: 'json',
            delay : 250,
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }

                return query;
            }
        },
        placeholder: 'Cari person',
        minimumInputLength: 3,
        
    },
}
var rst_riset={
	dt : null,
	frm : null,
        frm_sub : null,
        frm_mic : [],
	mdl_inp : $('#mdl_inp').my_plugins().page(),
	mdl_prf : $('#mdl_lht').my_plugins().page(),
	open_profile : function(par){
            var _this=this;
            this.mdl_prf.mdl_open({title:par.nama,clearbody:false});
            $.post(_page_info.url+'/get_riset',{id:par.id},function(respon){
                _this.mdl_prf.find('.modal-body').html(respon);
                _this.mdl_prf.enable_btn();
            }).fail(function(){
                _this.mdl_prf.notify('error',_page_info.title,_msg.unknow_error);
            })
	},
	open_dialog_input : function(mode,callback=null,par=null){
            var _this=this;
            this.frm_mic = [];
            this.mdl_inp.mdl_open({title:mode+' '+_page_info.title,clearbody:false});

            $.post(_page_info.url+'/get_frominput',par,function(respon){
                _this.mdl_inp.find('.modal-body').html(respon);
                _this.frm=$('form[id^="refdef-"]').my_plugins().form();

                $(this).find('select[name="inisiator"]').my_plugins().select2(template.sel2_ajax_conf);

                // FOR UPDATING
                if(callback=='open_ubah'){
                    par.jenis_riset = _this.frm.find('select[name="jenis"]').val();
                    _this.get_formbyjenis(par);
                }
                //---------------------------------------------
                
                //IF ONLY 1 JENIS AVAILABLE
                if(callback==null && _this.mdl_inp.find('select[name="jenis"] option').length==2){
                    _this.mdl_inp.find('select[name="jenis"]').trigger('change');
                }
                //---------------------------------------------
                _this.mdl_inp.enable_btn();
            }).fail(function(){
                    _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
            })
            _this.mdl_inp.unblock();
            _this.mdl_inp.enable_btn();
	},
        get_formbyjenis : function(par){
            var _this = this;
            _this.mdl_inp.block();
            $.post(_page_info.url+'/get_formByJenis',par,function(respon){
                _this.mdl_inp.find('div#panel_subform').html(respon);
                var _frmid = _this.mdl_inp.find('div#panel_subform').find('form').attr('id');
                _this.frm_sub = $('form#'+_frmid).my_plugins().form();
                
                _this.mdl_inp.find('#tab_skkon .tab-pane').each(function(){
                    var _id = uniqid();
                    $(this).find('form').attr('id',$(this).find('form').attr('id')+_id);
                    _this.frm_mic.push(_this.mdl_inp.find('form#rstsubsk'+_id).my_plugins().form());
                })
                
                _this.mdl_inp.find('table.tbrstkon').my_plugins().form();
                _this.mdl_inp.find('table.tbrstkon tbody tr').each(function(){
                    $(this).find('select[name="user_username"]').my_plugins().select2(template.sel2_ajax_conf);
                })
                var _sk = $('input[name="sk_existing"]').val();
                if(_sk!=''){
                    rst_skkon.clear_tab();
                    rst_skkon.add_sk(JSON.parse(_sk));
                }
                _this.mdl_inp.unblock();
            }).fail(function(){
                _this.mdl_inp.unblock();
                _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
            })
        },
        tb_str : {
            _this : this,
            tbl : 'table.tbrstkon',
            template : `<tr data-id="">
                        <td></td>
                        <td>
                            <select class="form-control" name="user_username" id="user_username_uniqid">
                                <option value="">-- Pilih Person --</option>
                            </select>
                        </td>
                        <td>
                            <select class="form-control my-select2" name="jenis">
                                `+opt.peran+`
                            </select>
                        </td>
                        <td>
                            <a href="javascript:;" data-aksi="delrow"><i class="fa fa-trash-o icon20"></i><a>
                            <a href="javascript:;" data-aksi="undoremove" style="display:none"><i class="fa fa-undo icon20"></i></a>
                        </td>
                    </tr>`,
            addrow : function(ins){
                var uniq = uniqid();
                var $tb = $(ins).closest('.tab-pane').find(this.tbl);
                $tb.find('tbody').append(this.template.replace(/uniqid/g, uniq));
                $tb.find('tbody tr:last').my_plugins().form();
                $('select#user_username_'+uniq).my_plugins().select2(template.sel2_ajax_conf);
                this.handle_rownum();
            },
            removerow : function(ins){
                var tr = $(ins).closest('tr');
                var id = tr.attr('data-id');
                if(typeof id == 'undefined' || id == null || id == ""){
                    tr.remove();
                    this.handle_rownum();
                }else{
                    tr.find('a[data-aksi="delrow"]').hide();
                    tr.find('a[data-aksi="undoremove"]').show(400);
                    tr.addClass('deleted');
                    tr.find('select').attr('disabled','disabled');
                }
            },
            undoremove : function(ins){
                var tr = $(ins).closest('tr');
                tr.find('a[data-aksi="undoremove"]').hide();
                tr.find('a[data-aksi="delrow"]').show(400);
                tr.removeClass('deleted');
                tr.find('select').removeAttr('disabled');
            },
            handle_rownum : function(){
                $(this.tbl).each(function(){
                    $(this).find('tbody tr').each(function(i){
                        $(this).find('td:first').html(i+1);
                    })
                })
                
            },
            serialize : function(id = null){
                var data = {data:[],removed:[]};
                if(id != null) var $tb = $(id); else var $tb = $(this.tbl);
                $tb.find('tbody tr').each(function(){
                    if($(this).hasClass('deleted')){
                        data.removed.push($(this).attr('data-id'));
                    }else{
                        var _data = {id:$(this).attr('data-id')};
                        $(this).find('select').each(function(){
                            _data[$(this).attr('name')] = $(this).val()
                        })
                        data.data.push(_data);
                    }
                })
                return data;
            }
        },
        serialize_sk : function(){
            var res = {data:[],deleted:[]};
            var _this = this;
            $.each(this.frm_mic,function(i,v){
                var id = $(_this.frm_mic[i]).closest('.tab-pane').attr('id');
                var _a = $('a[href="#'+id+'"]');
                
                if(_a.hasClass('deleted')){
                    var _obj = _this.frm_mic[i].serialize();
                    res.deleted.push(_obj.id);
                }else{
                    var _obj = _this.frm_mic[i].serialize();
                    _obj.kon = _this.tb_str.serialize($(_this.frm_mic[i].selector).closest('.tab-pane').find('table'));
                    if(typeof _obj.id != 'undefined') res.data.push(_obj);
                }
                
            })
            return res;
        },
	open_ubah : function(par){
            var _this=this;
            this.mdl_inp.mdl_open({title:'Ubah Riset '+par.nama,clearbody:false});
//            $.post(_page_info.url+'/get_ubahdata',{id:par.id},function(respon){
//                    _this.frm_bio.reset();
//                    _this.frm_usr.reset();
//
//                    _this.frm_bio.setdata(respon);
//                    _this.frm_usr.setdata(respon);
//                    $(_this.frm_usr.selector).find('[name="isAktif"]').trigger('change');
//
//                    _this.mdl_inp.unblock();
//                    _this.mdl_inp.enable_btn();	
//            },'json').fail(function(){
//                    _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
//            })
	},
	simpan : function(){
            var _this=this;
            if(this.frm.validate() && this.frm_sub.validate()){
                var is = true;
                $.each(this.frm_mic,function(i,v){
                    if(typeof _this.frm_mic[i] !== 'undefined')
                        is = _this.frm_mic[i].validate();
                })
                if(is){
                    this.mdl_inp.block();
                    this.mdl_inp.disable_btn();
                    var data={riset:this.frm.serialize(),prop:this.frm_sub.serialize(),sk:this.serialize_sk()}
                    //console.log(data);
                    $.post(_page_info.url+'/set_simpan',data,function(respon){
                            if(respon.isOk){
                                _this.mdl_inp.notify('success',_page_info.title,'Riset baru berhasil ditambahkan');
                                _this.mdl_inp.mdl_hide();
                                _this.dt.reload();
                            }else{
                                _this.mdl_inp.notify('error',_page_info.title,respon.msg);		 			
                            }
                            _this.mdl_inp.unblock();
                            _this.mdl_inp.enable_btn();
                    },'json').fail(function(){
                            _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
                            _this.mdl_inp.unblock();
                            _this.mdl_inp.enable_btn();
                    })                    
                }

            }
	},
	hapus : function(id){
            var _this=this;
            $.post(_page_info.url+'/set_hapus',{id:id},function(respon){
                if(respon.isOk){
                    _this.mdl_inp.notify('success',_page_info.title,'Riset berhasil dihapus.');	
                    _this.dt.reload();
                }else{
                    _this.mdl_inp.notify('error',_page_info.title,respon.msg);
                }
            },'json').fail(function(){
                _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
            })
	},
}
var rst_skkon = {
    _tab : '#tab_skkon',
    add_sk : function(idsk = []){
        var $tab = $(this._tab);
        $tab.find('li.active').removeClass('active');
        $tab.find('.tab-pane.active').removeClass('active');
        $tab.find('.nav-tabs').append(
            `<li class="active">
                <a href="#tab8_2" data-toggle="tab" aria-expanded="true"><span class="number"></span>. <span class="title">(SK Baru)</span> &nbsp;&nbsp; <i class="fa fa-trash-o icon20 clickable" data-aksi="del-sk"></i><i class="fa fa-undo icon20 clickable" data-aksi="del-undo" style="display:none"></i></a>
            </li>`);
        $tab.find('.tab-content').append(`<div id="" class="tab-pane active"></div>`);
        
        var _this = this;
        var $_tab = $(this._tab).my_plugins().page();
        $_tab.block();
        
        var par = {};
        if(idsk.length>0){
            par = {id:idsk[0]};
            idsk.shift();
        }
        
        $.post(_page_info.url+'/get_formSK',par,function(respon){
            var $_tac = $_tab.find('.tab-pane.active');
            $_tac.html(respon);
            var _id = uniqid();
            $_tac.find('form').attr('id',$_tac.find('form').attr('id')+_id);
            rst_riset.frm_mic.push($('form#rstsubsk'+_id).my_plugins().form());
            $_tac.find('table.tbrstkon').my_plugins().form();
            $_tac.find('table.tbrstkon tbody tr').each(function(){
                $(this).find('select[name="user_username"]').my_plugins().select2(template.sel2_ajax_conf);
            })
            $_tab.unblock();
            
            if(idsk.length>0) _this.add_sk(idsk);
            else _this.set_tabtitle();
        }).fail(function(){
            $_tab.unblock();
            $_tab.notify('error',_page_info.title,_msg.unknow_error);
        })
        this.handle_tabnum();
    },
    remove_sk : function(ins){
        var _this = this;
        var nav = $(ins).closest('li').find('a');
        var pane = $(this._tab).find('.tab-content div'+nav.attr('href'));
        var id = pane.find('input[name="id"]').val();
        
        if(id==''){
            var tabid = $(ins).closest('a').attr('href');
            var isblank = true;
            $(ins).closest('div.tab-block').find('div'+tabid).find('input,select').each(function(){
                var val = $(this).val();
                if(val!=''){
                    isblank = false;
                    return false;
                }
            });
            if(!isblank){
                bootbox.confirm('Apakah anda yakin mengabaikan perubahan dan melanjutkan menghapus SK ?',function(respon){
                    if(respon){
                        _this.do_remove_sk(ins,tabid);
                    }
                })
            }else{ _this.do_remove_sk(ins,tabid); }
        }else{
            nav.addClass('deleted');
            nav.find('i.fa-trash-o').hide();
            nav.find('i.fa-undo').show();
            pane.find('.panel-sk').hide();
            pane.append(`<div class="alert alert-info alert-dismissable">
                <i class="fa fa-info pr10"></i>
                Anda telah menghapus SK Riset, SK akan terhapus setelah Anda menekan tombol simpan. Anda dapat membatalkan penghapusan SK dengan menekan tombol undo</div>`);
        }
    },
    do_remove_sk : function(ins,tabid){
        var tab = $(ins).closest('.tab-block');
        tab.find('a[href="'+tabid+'"]').closest('li').remove();
        tab.find('div'+tabid).remove();
        tab.find('ul.nav li:last').addClass('active');
        tab.find('div.tab-content div.tab-pane:last').addClass('active');
    },
    undo_remove : function(ins){
        var nav = $(ins).closest('li').find('a');
        var pane = $(this._tab).find('.tab-content div'+nav.attr('href'));
        
        nav.removeClass('deleted');
        nav.find('i.fa-trash-o').show();
        nav.find('i.fa-undo').hide();
        pane.find('.panel-sk').show();
        pane.find('div.alert').remove();
    },
    handle_tabnum : function(){
        var _this = this;
        var $tab = $(this._tab);
        $tab.find('.nav-tabs li').each(function(i){
            $(this).find('a').attr('href',_this._tab+i.toString());
            $(this).find('a span.number').html(i+1);
        })
        $tab.find('.tab-content .tab-pane').each(function(i){
            $(this).attr('id',_this._tab.replace('#','')+i.toString());
        })
    },
    set_tabtitle : function(tab=null,title=null){
        var _this = this;
        if(tab==null && title==null){
            $(this._tab).find('.tab-pane').each(function(){
                var tab = $(this).attr('id');
                var title = $(this).find('input[name="no_sk"]').val();
                if(title=='') title = '(SK Baru)';
                $(_this._tab).find('.nav a[href="#'+tab+'"] span.title').html(title);    
            })    
        }else{
            $(this._tab).find('.nav a[href="#'+tab+'"] span.title').html(title);    
        }
        
    },
    clear_tab : function(){
        var _tab = $(this._tab);
        _tab.find('.nav-tabs li').remove();
        _tab.find('.tab-content').html('');
    }
}
$(document).ready(function(){
    rst_riset.dt=$('#main_dt').my_plugins().datatable({url:'/'+_page_info.classname+'/getdata'},['id']);
})
$('button[data-aksi="tambah"]').click(function(){
    rst_riset.open_dialog_input('Tambah');
})
$('body').on('change','form#refdef-rst_riset select[name="jenis"]',function(){
    rst_riset.get_formbyjenis({'jenis_riset':$(this).val()});
})
$('body').on('click','a[data-aksi="addrow_kon"]',function(){
    rst_riset.tb_str.addrow($(this));
})
$('body').on('click','a[data-aksi="delrow"]',function(){
    rst_riset.tb_str.removerow($(this));
})
$('body').on('click','a[data-aksi="undoremove"]',function(){
    rst_riset.tb_str.undoremove($(this));
})
rst_riset.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
    rst_riset.simpan();
})
$('body').on('click','a[data-aksi="addtab_skkon"]',function(){
    rst_skkon.add_sk();
})
$('body').on('change','input[name="no_sk"]',function(){
    var $tabid = $(this).closest('.tab-pane').attr('id');
    rst_skkon.set_tabtitle($tabid,$(this).val());
})
$('body').on('click','a[data-aksi="rst-del"]',function(){
    var par=JSON.parse(urldecode($(this).attr('data-par')));
    bootbox.confirm('Apakah anda yakin akan menghapus riset dengan judul <b>'+par.nama+'</b> ?',function(respon){
        if(respon) rst_riset.hapus(par.id);
    })
})
$('body').on('click','a[data-aksi="rst-ubah"]',function(){
    var par=JSON.parse(urldecode($(this).attr('data-par')));
    rst_riset.open_dialog_input('Ubah','open_ubah',par);
})
$('body').on('click','a[data-aksi="rst-lihat"]',function(){
    var par=JSON.parse(urldecode($(this).attr('data-par')));
    rst_riset.open_profile(par);
})
$('body').on('click','i[data-aksi="del-sk"]',function(){
    rst_skkon.remove_sk($(this));
})
$('body').on('click','i[data-aksi="del-undo"]',function(){
    rst_skkon.undo_remove($(this));
})