var reqbimbing = {
    dt : null,
    mdl_lht : $('#mdl_lht').my_plugins().page(),
    mdl_inp : $('#mdl_inp').my_plugins().page(),
    frm_res : null,
    lihat_rst : function(par){
        var _this=this;
        this.mdl_lht.mdl_open({title:par.nama,clearbody:false});
        $.post(_page_info.url+'/get_riset',{id:par.id},function(respon){
            _this.mdl_lht.find('.modal-body').html(respon);
            _this.mdl_lht.enable_btn();
        }).fail(function(){
            _this.mdl_lht.enable_btn();
            _this.mdl_lht.notify('error',_page_info.title,_msg.unknow_error);
        })
    },
    respon_req : function(par){
        var _this=this;
        this.mdl_inp.mdl_open({title:par.nama,clearbody:false});
        $.post(_page_info.url+'/get_formrespon',{id:par.id},function(respon){
            _this.mdl_inp.find('.modal-body').html(respon);
            _this.frm_res = $('form#rst-rst_reqbimbing').my_plugins().form();
            _this.mdl_inp.enable_btn();
        }).fail(function(){
            _this.mdl_lht.enable_btn();
            _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
        })
    },
    respon_simpan : function(){
        var _this=this;
        
        if(_this.frm_res.validate()){
            var data = _this.frm_res.serialize();
            
            $.post(_page_info.url+'/set_simpankontribusi',data,function(respon){
                if(respon.isOk){
                    _this.mdl_inp.notify('success',_page_info.title,'Berhasil menyimpan '+_page_info.title);
                    _this.mdl_inp.mdl_hide();
                    _this.dt.reload();
                }else{
                    _this.mdl_inp.notify('error',_page_info.title,respon.msg);
                }
                _this.mdl_inp.enable_btn();
            },'json').fail(function(){
                _this.mdl_inp.enable_btn();
                _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
            })
        
        }
    },
}
$(document).ready(function(){
    reqbimbing.dt=$('#main_dt').my_plugins().datatable({url:'/'+_page_info.classname+'/getdata'},['id']);
})
$('body').on('click','a[data-aksi="rst-lihat"]',function(){
    var par = JSON.parse(urldecode($(this).attr('data-par')));
    reqbimbing.lihat_rst(par);
})
$('body').on('click','a[data-aksi="req-respon"]',function(){
    var par = JSON.parse(urldecode($(this).attr('data-par')));
    reqbimbing.respon_req(par);
})
reqbimbing.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
    reqbimbing.respon_simpan();
})