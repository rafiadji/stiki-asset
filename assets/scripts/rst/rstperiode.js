var rst_periode={
    dt : null,
    frm_per : null,
    frm_sub : null,
    mdl_inp : $('#mdl_inp').my_plugins().page(),
    mdl_lht : $('#mdl_lht').my_plugins().page(),
    open_dialog_lihat : function(par){
        var _this=this;
        this.mdl_lht.mdl_open({title:par.nama,clearbody:false});
        
        $.post(_page_info.url+'/get_lihat',par,function(respon){
            _this.mdl_lht.find('.modal-body').html(respon);
            _this.mdl_lht.enable_btn();
        }).fail(function(){
                _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
        })
    },
    open_dialog_input : function(mode,callback=null,par=null){
        var _this=this;
        this.frm_per = [];
        this.mdl_inp.mdl_open({title:mode+' '+_page_info.title,clearbody:false});

        $.post(_page_info.url+'/get_frominput',par,function(respon){
            _this.mdl_inp.find('.modal-body').html(respon);
            _this.frm=$('form[id^="refdef-"]').my_plugins().form();

            // FOR UPDATING
            if(callback=='open_ubah'){
                par.jenis_riset = _this.frm.find('select[name="jenis"]').val();
                _this.get_formbyjenis(par);
            }
            //---------------------------------------------

            //IF ONLY 1 JENIS AVAILABLE
            if(callback==null && _this.mdl_inp.find('select[name="jenis"] option').length==2){
                _this.mdl_inp.find('select[name="jenis"]').trigger('change');
            }
            //---------------------------------------------
            _this.mdl_inp.enable_btn();
        }).fail(function(){
                _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
        })
        _this.mdl_inp.unblock();
        _this.mdl_inp.enable_btn();
    },
    get_formbyjenis : function(par){
        var _this = this;
        _this.mdl_inp.block();
        $.post(_page_info.url+'/get_formByJenis',par,function(respon){
            _this.mdl_inp.find('div#panel_subform').html(respon);
            var _frmid = _this.mdl_inp.find('div#panel_subform').find('form').attr('id');
            _this.frm_sub = $('form#'+_frmid).my_plugins().form();

            _this.mdl_inp.unblock();
        }).fail(function(){
            _this.mdl_inp.unblock();
            _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
        })
    },
    set_aktiftoggle_change : function(elm){
        var _this=this;
        var par=JSON.parse(urldecode($(elm).attr('data-par')));
        if($(elm).is(':checked')) var isCheck='YES'; else var isCheck='NO';
        $.post(_page_info.url+'/set_aktifchange',{id:par.idthp,aktif:isCheck},function(respon){
            if(respon.isOk){
                _this.mdl_inp.notify('success',_page_info.title,'Berhasil merubah status keaktifan '+_page_info.title+'.');
            }else{
                _this.mdl_inp.notify('error',_page_info.title,respon.msg);
            }
        },'json').fail(function(){
            _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
        })	
    },
    simpan : function(){
        var _this=this;
        
        this.mdl_inp.block();
        this.mdl_inp.disable_btn();
        if(this.mdl_inp.validate()){
            var data = this.mdl_inp.serialize();
            $.post(_page_info.url+'/set_simpan',data,function(respon){
                if(respon.isOk){
                    _this.mdl_inp.notify('success',_page_info.title,_page_info.title+' baru berhasil ditambahkan');
                    _this.mdl_inp.mdl_hide();
                    _this.dt.reload();
                }else{
                    _this.mdl_inp.notify('error',_page_info.title,respon.msg);		 			
                }
                _this.mdl_inp.unblock();
                _this.mdl_inp.enable_btn();
            },'json').fail(function(){
                _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
                _this.mdl_inp.unblock();
                _this.mdl_inp.enable_btn();
            })
        }else{
            _this.mdl_inp.notify('error',_page_info.title,'Lengkapi isian yang kurang tepat');
            _this.mdl_inp.unblock();
            _this.mdl_inp.enable_btn();
        }
    },
    hapus : function(id){
        var _this=this;
        $.post(_page_info.url+'/set_hapus',{id:id},function(respon){
            if(respon.isOk){
                _this.mdl_inp.notify('success',_page_info.title,_page_info.title+' berhasil dihapus.');	
                _this.dt.reload();
            }else{
                _this.mdl_inp.notify('error',_page_info.title,respon.msg);
            }
        },'json').fail(function(){
            _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
        })
    },
    open_ubah : function(par){
        this.open_dialog_input('ubah','open_ubah',par);
    },
}
$(document).ready(function(){
    rst_periode.dt=$('#main_dt').my_plugins().datatable({url:'/'+_page_info.classname+'/getdata'},['id']);
})
$('button[data-aksi="tambah"]').click(function(){
    rst_periode.open_dialog_input('Tambah');
})
$('body').on('change','form#refdef-rst_periode select[name="jenis"]',function(){
    rst_periode.get_formbyjenis({'jenis_riset':$(this).val()});
})
$('body').on('change','input[name="refdef_isaktif"]',function(){
    rst_periode.set_aktiftoggle_change($(this));
})
rst_periode.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
    rst_periode.simpan();
})
$('body').on('click','a[data-aksi="rstper-del"]',function(){
    var par=JSON.parse(urldecode($(this).attr('data-par')));
    bootbox.confirm('Apakah anda yakin akan menghapus '+_page_info.title+' <b>'+par.nama+'</b> ?',function(respon){
        if(respon) rst_periode.hapus(par.id);
    })
})
$('body').on('click','a[data-aksi="rstper-ubah"]',function(){
    var par=JSON.parse(urldecode($(this).attr('data-par')));
    rst_periode.open_ubah(par);
})
$('body').on('click','a[data-aksi="rstper-lihat"]',function(){
    var par=JSON.parse(urldecode($(this).attr('data-par')));
    rst_periode.open_dialog_lihat(par);
})