var reqseminar = {
    dt : null,
    mdl_lht : $('#mdl_lht').my_plugins().page(),
    mdl_inp : $('#mdl_inp').my_plugins().page(),
    frm_res : null,
    respon_req : function(par){
        var _this=this;
        this.mdl_inp.mdl_open({title:par.nama,clearbody:false});
        $.post(_page_info.url+'/get_formrespon',{id:par.id},function(respon){
            _this.mdl_inp.find('.modal-body').html(respon);
            _this.frm_res = $('form#rst-rst_reqseminar').my_plugins().form();
            _this.mdl_inp.enable_btn();
        }).fail(function(){
            _this.mdl_lht.enable_btn();
            _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
        })
    },
    respon_simpan : function(){
        var _this=this;
        
        if(_this.frm_res.validate()){
            var data = _this.frm_res.serialize();
            
            $.post(_page_info.url+'/set_simpankontribusi',data,function(respon){
                if(respon.isOk){
                    _this.mdl_inp.notify('success',_page_info.title,'Berhasil menyimpan '+_page_info.title);
                    _this.mdl_inp.mdl_hide();
                    _this.dt.reload();
                }else{
                    _this.mdl_inp.notify('error',_page_info.title,respon.msg);
                }
                _this.mdl_inp.enable_btn();
            },'json').fail(function(){
                _this.mdl_inp.enable_btn();
                _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
            })
        
        }
    },
    init : function(){
        this.dt=$('#main_dt').my_plugins().datatable({url:'/'+_page_info.classname+'/getdata'},['id']);
    }
}
$(document).ready(function(){
    reqseminar.init();
})
$('body').on('click','a[data-aksi="req-respon"]',function(){
    var par = JSON.parse(urldecode($(this).attr('data-par')));
    reqseminar.respon_req(par);
})
reqseminar.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
    reqseminar.respon_simpan();
})