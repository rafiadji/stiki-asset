var template = {
    sel2_ajax_conf: {
        ajax: {
            url: _page_info.url + '/get_optUser',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }

                return query;
            }
        },
        placeholder: 'Cari person',
        minimumInputLength: 3,

    },
}
var bimbingan = {
    tb_bimbing : null,
    tb_peserta : null,
    dt: null,
    frm: null,
    frm_sub: null,
    frm_mic: [],
    mdl_inp: $('#mdl_inp').my_plugins().page(),
    mdl_prf: $('#mdl_lht').my_plugins().page(),
    mdl_prf_sub: $('#mdl_lht_sub').my_plugins().page(),
    mdl_inp_sub: $('#mdl_inp_sub').my_plugins().page(),
    open_summary: function () {
        var _this = this;
        $.post(_page_info.url + '/summary', function (respon) {
            $('.vw').html(respon);
        })
    },
    open_dialog_input: function (mode, callback = null, par = null) {
        var _this = this;
        this.frm_mic = [];
        this.mdl_inp.mdl_open({ title: mode + ' ' + _page_info.title, clearbody: false });

        $.post(_page_info.url + '/form_bimbingan', par, function (respon) {
            _this.mdl_inp.find('.modal-body').html(respon);
            _this.frm = $('form[id^="refdef-"]').my_plugins().form();

            _this.mdl_inp.enable_btn();
        }).fail(function () {
            _this.mdl_inp.notify('error', _page_info.title, _msg.unknow_error);
        })
        _this.mdl_inp.unblock();
        _this.mdl_inp.enable_btn();
    },
    open_input_respon: function (mode, callback = null, par = null) {
        var _this = this;
        this.frm_mic = [];
        this.mdl_inp_sub.mdl_open({ title: mode + ' ' + _page_info.title, clearbody: false });

        $.post(_page_info.url + '/detail_bimbingan', par, function (respon) {
            _this.mdl_inp_sub.find('.modal-body').html(respon);
            _this.frm = $('form[id^="refdef-"]').my_plugins().form();

            _this.mdl_inp_sub.enable_btn();
        }).fail(function () {
            _this.mdl_inp_sub.notify('error', _page_info.title, _msg.unknow_error);
        })
        _this.mdl_inp_sub.unblock();
        _this.mdl_inp_sub.enable_btn();
    },
    open_dialog_detail_input: function (mode, callback = null, par = null) {
        var _this = this;
        this.frm_mic = [];
        this.mdl_prf_sub.mdl_open({ title: mode + ' ' + _page_info.title, clearbody: false });

        $.post(_page_info.url + '/detail_bimbingan', par, function (respon) {
            _this.mdl_prf_sub.find('.modal-body').html(respon);

            _this.mdl_prf_sub.enable_btn();
        }).fail(function () {
            _this.mdl_prf_sub.notify('error', _page_info.title, _msg.unknow_error);
        })
        _this.mdl_prf_sub.unblock();
        _this.mdl_prf_sub.enable_btn();
    },
    open_detail: function (mode, callback = null, par = null) {
        var _this = this;
        this.frm_mic = [];
        this.mdl_prf.mdl_open({ title: mode + ' ' + _page_info.title, clearbody: false });

        $.post(_page_info.url + '/tb_bimbingan', par, function (respon) {
            _this.mdl_prf.find('.modal-body').html(respon);
            $('#tb_bimbingan').my_plugins().datatable();
            $('button[data-aksi="detail-form"]').click(function () {
                bimbingan.open_input_respon('Detail', null, {'id':$(this).data("id")});
            })
            $('button[data-aksi="lihat_kontribusi"]').click(function(){
                bimbingan.open_lihat({id:$(this).attr('data-id')});
            })
            _this.mdl_prf.enable_btn();
        }).fail(function () {
            _this.mdl_prf.notify('error', _page_info.title, _msg.unknow_error);
        })
        _this.mdl_prf.unblock();
        _this.mdl_prf.enable_btn();
    },
    open_lihat: function(par){
        var _this = this;
        this.mdl_prf_sub.mdl_open({ title: 'Lihat Bimbingan', clearbody: false });

        $.post(_page_info.url + '/lihat_bimbingan', par, function (respon) {
            _this.mdl_prf_sub.find('.modal-body').html(respon);
            _this.mdl_prf_sub.enable_btn();
        }).fail(function () {
            _this.mdl_prf_sub.notify('error', _page_info.title, _msg.unknow_error);
        })
        _this.mdl_prf_sub.unblock();
        _this.mdl_prf_sub.enable_btn();
    },
    simpan: function () {
        var _this = this;
        if (this.frm.validate()) {
            this.mdl_inp.block();
            this.mdl_inp.disable_btn();
            var data = this.frm.serialize();

            $.post(_page_info.url + '/set_simpan_bimbingan', data, function (respon) {
                if (respon.isOk) {
                    _this.mdl_inp.notify('success', _page_info.title, 'Riset baru berhasil ditambahkan');
                    _this.mdl_inp.mdl_hide();
                    window.location.reload();
                } else {
                    _this.mdl_inp.notify('error', _page_info.title, respon.msg);
                }
                _this.mdl_inp.unblock();
                _this.mdl_inp.enable_btn();
            }, 'json').fail(function () {
                _this.mdl_inp.notify('error', _page_info.title, _msg.unknow_error);
                _this.mdl_inp.unblock();
                _this.mdl_inp.enable_btn();
            })
        }
    },
    simpan_membimbing : function(){
        var _this = this;
        if (this.frm.validate()) {
            this.mdl_inp_sub.block();
            this.mdl_inp_sub.disable_btn();
            var data = this.frm.serialize();

            $.post(_page_info.url + '/set_simpan_membimbing', data, function (respon) {
                if (respon.isOk) {
                    _this.mdl_inp_sub.notify('success', _page_info.title, 'Riset baru berhasil ditambahkan');
                    _this.mdl_inp_sub.mdl_hide();
                    window.location.reload();
                } else {
                    _this.mdl_inp_sub.notify('error', _page_info.title, respon.msg);
                }
                _this.mdl_inp_sub.unblock();
                _this.mdl_inp_sub.enable_btn();
            }, 'json').fail(function () {
                _this.mdl_inp_sub.notify('error', _page_info.title, _msg.unknow_error);
                _this.mdl_inp_sub.unblock();
                _this.mdl_inp_sub.enable_btn();
            })
        }
    },
    hapus: function (id) {
        var _this = this;
        $.post(_page_info.url + '/set_hapus', { id: id }, function (respon) {
            if (respon.isOk) {
                _this.mdl_inp.notify('success', _page_info.title, 'Riset berhasil dihapus.');
                _this.dt.reload();
            } else {
                _this.mdl_inp.notify('error', _page_info.title, respon.msg);
            }
        }, 'json').fail(function () {
            _this.mdl_inp.notify('error', _page_info.title, _msg.unknow_error);
        })
    },
}
$(document).ready(function () {
    bimbingan.tb_bimbing = $('#tb_bimbingan').my_plugins().datatable();
    bimbingan.tb_peserta = $('#tb_peserta').my_plugins().datatable();
    // rst_riset.dt = $('#main_dt').my_plugins().({ url: '/' + _page_info.classname + '/summary' }, ['id']);
    // rst_riset.open_icheck();
    //rst_ta.open_summary();
})
$('button[data-aksi="tambah"]').click(function () {
    bimbingan.open_dialog_input('Tambah');
})
$('button[data-aksi="detail-form"]').click(function () {
    bimbingan.open_dialog_detail_input('Detail', null, {'id':$(this).data("id")});
})
$('button[data-aksi="detail-bimbingan"]').click(function () {
    bimbingan.open_detail('Detail', null, {'id':$(this).data("id"), 'person':$(this).data("person"), 'person_ref':$(this).data("ref")});
})
bimbingan.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
    bimbingan.simpan();
})
bimbingan.mdl_inp_sub.find('button[data-aksi="simpan"]').click(function(){
    bimbingan.simpan_membimbing();
})
$('button[data-aksi="lihat_kontribusi"]').click(function(){
    bimbingan.open_lihat({id:$(this).attr('data-id')});
})