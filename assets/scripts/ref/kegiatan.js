var activity = {
  frm_act : null,
  mdl_inpreg : $('#mdl_inpreg').my_plugins().page(),
  get_formByJenis : function(par){
    var _this = this;
    ref_def.mdl_inp.find('div#panel_formact').remove();
    if(par.jenis_aktivitas == '') return false;
    
    ref_def.mdl_inp.block();
    $.post(_page_info.url+'/get_formByJenis',par,function(respon){
      ref_def.mdl_inp.find('.modal-body').append(respon);
      _this.frm_act = ref_def.mdl_inp.find('form#refact-rf_kegiatan').my_plugins().form();
      ref_def.mdl_inp.my_plugins().adminpanel();
      ref_def.mdl_inp.unblock();
    }).fail(function(){
      ref_def.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
    })
  },
  get_viewreg : function(par){
    ref_def.mdl_prf.find('.modal-title').html('Peserta '+par.nama);
    ref_def.mdl_prf.find('.modal-body').html('');
    ref_def.mdl_prf.modal('show');
    ref_def.mdl_prf.block();
    $.post(_page_info.url+'/get_peserta',par,function(respon){
        ref_def.mdl_prf.find('.modal-body').html(respon);
        ref_def.mdl_prf.find('.modal-body').find('table').my_plugins().datatable();
        ref_def.mdl_prf.unblock();
    }).fail(function(){
        ref_def.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
    })
  },
  open_inputreg : function(par){
    var _this = this;
    this.mdl_inpreg.mdl_open({title:'Ubah Peserta '+par.nama,clearbody:false});
    $.post(_page_info.url+'/get_inputreg',par,function(respon){
        _this.mdl_inpreg.find('.modal-body').html(respon);
        _this.mdl_inpreg.find('table#tableregact').my_plugins().form();
        
        _this.mdl_inpreg.find('table#tableregact').find('tbody td[data-used="form"]').each(function(){
            $(this).find('input,select').val($(this).attr('data-value')).trigger('change');
        })
        
        _this.handle_inputreg_fbulk();
        
        _this.mdl_inpreg.unblock();
        _this.mdl_inpreg.enable_btn();
    }).fail(function(){
        _this.mdl_inpreg.unblock();
        _this.mdl_inpreg.enable_btn();
        _this.mdl_inpreg.notify('error',_page_info.title,_msg.unknow_error);
    })
  },
  simpan_inputreg : function(ins){
      this.mdl_inpreg.block();
      this.mdl_inpreg.disable_btn();
      
      var _this = this;
      var table = $(ins).closest('.modal').find('.modal-body table');
      var data = [];
      table.find('tbody tr').each(function(){
          var regid = $(this).attr('data-regid');
          $(this).find('td[data-used="form"]').find('select,input').each(function(){
            data.push({'peserta':regid,'attribute':$(this).attr('name'),'value':$(this).val()});  
          })
      })
      
      $.post(_page_info.url+'/set_simpanattrreg',{data:data},function(respon){
        if(respon.isOk){
            _this.mdl_inpreg.notify('success',_page_info.title,'Data pendaftaran peserta kegiatan berhasil tersimpan');
            _this.mdl_inpreg.mdl_hide();
            _this.dt.reload();
        }else{
            _this.mdl_inpreg.notify('error',_page_info.title,respon.msg);
        }
        _this.mdl_inpreg.unblock();
        _this.mdl_inpreg.enable_btn();
      },'json').fail(function(){
            _this.mdl_inpreg.unblock();
            _this.mdl_inpreg.enable_btn();
            _this.mdl_inpreg.notify('error',_page_info.title,_msg.unknow_error);
      })
  },
  handle_inputreg_fbulk : function(ins=null){
      var is = false;
      if(ins == null) var table = this.mdl_inpreg.find('table');
      else var table = $(ins).closest('table');
      
      table.find('tbody tr input.rowregact-sel').each(function(){
          if($(this).is(':checked')){ is = true; return false};
      })
      if(is) table.find('thead th[data-used="form"] div.ingroup').show(400);
      else table.find('thead th[data-used="form"] div.ingroup').hide(400);
  }
}

activity.mdl_inpreg.find('button[data-aksi="simpan"]').click(function(){
    activity.simpan_inputreg($(this));
})

$('body').on('change','select[name="jenis_aktivitas"]',function(){
  activity.get_formByJenis(ref_def.frm_def.serialize());
})
$('body').on('click','a[data-aksi="ref-viewreg"]',function(){
  var _par = JSON.parse(urldecode($(this).attr('data-par')));
  activity.get_viewreg(_par);
})
$('body').on('click','a[data-aksi="ref-ubahreg"]',function(){
  var _par = JSON.parse(urldecode($(this).attr('data-par')));
  activity.open_inputreg(_par);
})
$('body').on('change','input#rowregact-all',function(){
    var _this = this;
    $(this).closest('table').find('tbody tr input.rowregact-sel').each(function(){
        if(!$(_this).is(':checked'))
            $(this).attr('checked','checked').trigger('click');
        else $(this).removeAttr('checked').trigger('click');
    })
})
$('body').on('change','input.rowregact-sel',function(){
    activity.handle_inputreg_fbulk($(this));
})
$('body').on('change','th[data-used="form"] select,th[data-used="form"] input',function(){
    if($(this).attr('type')=='text') return false;
    var _this = this;
    $(this).closest('table').find('tbody [name="'+$(this).attr('name')+'"]').each(function(){
        if($(this).closest('tr').find('td:first input[type="checkbox"]').is(':checked'))
            $(this).val($(_this).val()).trigger('change');
    })
})
$('body').on('keyup','th[data-used="form"] input[type="text"]',function(){
    var _this = this;
    $(this).closest('table').find('tbody [name="'+$(this).attr('name')+'"]').each(function(){
        if($(this).closest('tr').find('td:first input[type="checkbox"]').is(':checked'))
            $(this).val($(_this).val()).trigger('change');
    })
})