var ref_def={
	dt : null,
	frm_def : null,
	mdl_inp : $('#mdl_inp').my_plugins().page(),
	mdl_prf : $('#mdl_lht').my_plugins().page(),
	open_profile : function(par){
		var _this=this;
		this.mdl_prf.mdl_open({title:'Profile '+_page_info.title+' '+par.nama,clearbody:false});
		$.post(_page_info.url+'/get_profile',{id:par.id},function(respon){
			_this.mdl_prf.find('.modal-body').html(respon);
			_this.mdl_prf.enable_btn();
		}).fail(function(){
			_this.mdl_prf.notify('error',_page_info.title,_msg.unknow_error);
		})
	},
	open_dialog_input : function(mode,callback=null,par=null){
		var _this=this;
		this.mdl_inp.mdl_open({title:'Tambah '+_page_info.title,clearbody:false});
		if(_this.frm_def==null){
			$.post(_page_info.url+'/get_frominput',function(respon){
				_this.mdl_inp.find('.modal-body').html(respon);
				_this.frm_def=$('form[id^="refdef-"]').my_plugins().form();
				
				if(callback=='open_ubah') _this.open_ubah(par);
				_this.mdl_inp.enable_btn();
			}).fail(function(){
				_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
			})
		}else{
			_this.frm_def.reset();

			_this.mdl_inp.unblock();
			_this.mdl_inp.enable_btn();
		}
	},
	open_ubah : function(par){
		var _this=this;
		if(this.frm_def==null){
			this.open_dialog_input('ubah','open_ubah',par);
		}else{
			this.mdl_inp.mdl_open({title:'Ubah '+_page_info.title+' '+par.nama,clearbody:false});
			$.post(_page_info.url+'/get_ubahdata',{id:par.id},function(respon){
				_this.frm_def.reset();

				_this.frm_def.setdata(respon);

				_this.mdl_inp.unblock();
				_this.mdl_inp.enable_btn();	
			},'json').fail(function(){
				_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
			})
		}
	},
	simpan : function(){
		var _this=this;
		if(this.frm_def.validate()){
			this.mdl_inp.block();
			this.mdl_inp.disable_btn();
			if(this.mdl_inp.validate()){
				var data = this.mdl_inp.serialize();
			 	$.post(_page_info.url+'/set_simpan',data,function(respon){
			 		if(respon.isOk){
			 			_this.mdl_inp.notify('success',_page_info.title,_page_info.title+' baru berhasil ditambahkan');
			 			_this.mdl_inp.mdl_hide();
			 			_this.dt.reload();
			 		}else{
						_this.mdl_inp.notify('error',_page_info.title,respon.msg);		 			
			 		}
			 		_this.mdl_inp.unblock();
			 		_this.mdl_inp.enable_btn();
			 	},'json').fail(function(){
			 		_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
			 		_this.mdl_inp.unblock();
			 		_this.mdl_inp.enable_btn();
			 	})
			}else{
				_this.mdl_inp.notify('error',_page_info.title,'Lengkapi isian yang kurang tepat');
				_this.mdl_inp.unblock();
			 	_this.mdl_inp.enable_btn();
			}
		}
	},
	hapus : function(id){
		var _this=this;
		$.post(_page_info.url+'/set_hapus',{id:id},function(respon){
			if(respon.isOk){
				_this.mdl_inp.notify('success',_page_info.title,_page_info.title+' berhasil dihapus.');	
				_this.dt.reload();
			}else{
				_this.mdl_inp.notify('error',_page_info.title,respon.msg);
			}
		},'json').fail(function(){
			_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		})
	},
	set_aktiftoggle_change : function(elm){
		var _this=this;
		var par=JSON.parse(urldecode($(elm).attr('data-par')));
		if($(elm).is(':checked')) var isCheck='YES'; else var isCheck='NO';
		$.post(_page_info.url+'/set_aktifchange',{id:par.id,aktif:isCheck},function(respon){
			if(respon.isOk){
				_this.mdl_inp.notify('success',_page_info.title,'Berhasil merubah status keaktifan '+_page_info.title+'.');
			}else{
				_this.mdl_inp.notify('error',_page_info.title,respon.msg);
			}
		},'json').fail(function(){
			_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		})	
	}
}
$(document).ready(function(){
	ref_def.dt=$('#main_dt').my_plugins().datatable({url:'/'+_page_info.classname+'/getdata'},['id']);
})
$('button[data-aksi="tambah"]').click(function(){
	ref_def.open_dialog_input('Tambah');
})
ref_def.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
	ref_def.simpan();
})
$('body').on('click','a[data-aksi="ref-del"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	bootbox.confirm('Apakah anda yakin akan menghapus '+_page_info.title+' <b>'+par.nama+'</b> ?',function(respon){
		if(respon) ref_def.hapus(par.id);
	})
})
$('body').on('change','input[name="refdef_isaktif"]',function(){
	ref_def.set_aktiftoggle_change($(this));
})
$('body').on('click','a[data-aksi="ref-ubah"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	ref_def.open_ubah(par);
})
$('body').on('click','a[data-aksi="ref-lihat"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	ref_def.open_profile(par);
})