$(document).ready(function(){
  myact.init();
})
$('#tab_myact').on('click','.act-tile',function(){
  myact.load_act($(this));
})
$('#actmy_act a[data-aksi="act_nav_back"]').click(function(){
  myact.back_to_list();
})
var myact = {
  pnl_lst : $('#tab_myact'),
  pnl_act : $('#actmy_act'),
  init : function(){
    this.pnl_act = this.pnl_act.my_plugins().page();
  },
  load_act : function(ins){
    var _this = this;
    this.pnl_lst.hide();
    this.pnl_act.show();
    this.pnl_act.block();
    $.get(_page_info.url+'/kegiatan/'+$(ins).attr('data-id'),{ajax:true},function(respon){
      _this.pnl_act.find('#actdaftar_act_content').hide();
      _this.pnl_act.find('#actdaftar_act_content').html(respon);
      setTimeout(function(){
        act_layout.init();
        _this.pnl_act.unblock();
        _this.pnl_act.find('#actdaftar_act_content').show(300);
        $("html, body").animate({ scrollTop: 0 }, 600);
      },300);
    }).fail(function(){
      _this.pnl_act.notify('error',_page_info.title, _msg.unknow_error);
    })
  },
  back_to_list : function(){
    this.pnl_act.hide();
    this.pnl_lst.show(300);
  }
}