$('body').on('click','a[data-aksi="reg_act"]',function(){
  var par = htmlGetAllAttr($(this));
  bootbox.dialog({
    message:'<p>Dengan menekan tombol daftar, Saya menyatakan telah membaca, mengerti dan setuju terhadap syarat dan ketentuan yang mengikat pada kegiatan '+par['data-jenis']+' <b>&quot;'+par['data-nama']+'&quot;</b>. </p>',
    onEscape:true,
    backdrop:true,
    buttons:{
      cancel:{
        label:'<i class="glyphicons glyphicons-remove_2 icon-lg"></i>&nbsp;Batal',
        className:'btn-warning',
        callback:function(){
          
        }
      },
      regmein:{
        label:'<i class="glyphicons glyphicons-cart_in icon-lg"></i>&nbsp;Ya, Daftarkan Diri Saya',
        className:'btn-primary',
        callback:function(){
          var _body = $('section#content').my_plugins().page();
          _body.block();
          $.post(_page_info.url+'/regmein',{act:par['data-id']},function(respon){
            if(respon.isOk){
              _body.notify('success',_page_info.title,'Pendaftaran Anda telah berhasil tersimpan');
              setTimeout(function(){
                redirect(_site_url+'/act_my/kegiatan/'+par['data-id']);
              },1000)
            }else{
              _body.notify('error',_page_info.title,respon.msg);
            }
            _body.unblock();
          },'json').fail(function(){
            _body.unblock();
            _body.notify('error',_page_info.title,_msg.unknow_error);
          })
        }
      }
    }
  });
})
