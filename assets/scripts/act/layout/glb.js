$(document).ready(function(){
  act_layout.init();
})
var act_layout = {
  init : function(){
    if(typeof $('#myqrcode') !== 'undefined' && $('#myqrcode').length>0){
      var qrcode = new QRCode(document.getElementById("myqrcode"), {
        text: "http://jindo.dev.naver.com/collie",
        colorDark : "#000000",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.H
      });
    }
  }
}