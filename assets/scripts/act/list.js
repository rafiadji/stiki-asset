var calendar = null;
var $pnl_flter = $('#actdaftar_panel_filter');
var $pnl_lst = $('#actdaftar_panel_list');
var $pnl_act = $('#actdaftar_act');
$(document).ready(function () {
  $('div#actdaftar_panel').my_plugins().adminpanel();

  var calendarEl = document.getElementById('act_calendar');
  calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: ['dayGrid', 'timeGrid'],
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
    },
    //defaultView: 'month',
    height: 500,
    eventLimit: true,
  });
  actdaftar.get_act();
  actcalendar.get_act();
  $pnl_act = $pnl_act.my_plugins().page();
})
$('a[href="#tab17_1"]').click(function () {
  actdaftar.clear_act();
  setTimeout(function(){ actdaftar.get_act(); }, 300);
})
$('a[href="#tab17_2"]').click(function () {
  calendar.render();
  
})
$('a[href="#tab17_3"]').click(function () {
  $pnl_lst.find('.tab-pane').removeClass('active');
  $pnl_lst.find('#tab17_1').addClass('active');
  actdaftar.clear_act();
  setTimeout(function(){ actdaftar.get_act(); }, 300);
})
$('#act_calendar').on('click','button.fc-button',function(){
  actcalendar.get_act();
})
var actcalendar = {
  loaded : [],
  loaded_evt : [],
  get_act : function(){
    var _this = this;
    var _now = calendar.getDate();
    cur = (_now.getFullYear())+'-'+(_now.getMonth()+1);
    
    if(!in_array(cur,this.loaded)){
      $.get(_page_info.url+'/get_evtCalendar',{month:cur},function(respon){
          $.each(respon,function(i,v){
            if(!in_array(v.id,_this.loaded_evt)){
              calendar.addEvent(v);
              _this.loaded_evt.push(v.id);
            }
          })
          _this.loaded.push(cur);
          calendar.render();
      },'json').fail(function(){
        $pnl_act.notify('error',_page_info.title, _msg.unknow_error);
      })
    }
  }
}
var actdaftar = {
  pnl_tags: $("#actdaftar_filter_active"),
  pnl_card: $("#actdaftar_card_acts"),
  get_act : function(){
    var _this = this;
    var key = this.pnl_tags.find('span.tm-tag[data-kat="search"]').attr('data-id');
    var offset = _this.pnl_card.find('div.panel').length;
    if($pnl_lst.find('ul.nav li.active a').attr('href')=='#tab17_3') var mode = 'tile';
    else var mode = 'list';
    $.get(_page_info.url+'/get_listOfUserRegKegiatan',{offset:offset,key:key,mode:mode},function(respon){
        _this.pnl_card.append(respon);
        _this.refreshFilter(offset);
        _this.toogleLoadMore();
    }).fail(function(){
      $pnl_act.notify('error',_page_info.title, _msg.unknow_error);
    })
  },
  clear_act : function(){
    this.pnl_card.html('');
  },
  showFilterPanel: function () {
    $pnl_lst.closest('div.col').removeClass('col-lg-12').addClass('col-lg-9 col-md-8 col-sm-12')
    $pnl_flter.closest('div.col').show('300');
    $pnl_flter.removeClass('panel-collapsed');
    $pnl_flter.find('.panel-body').show();
  },
  hideFilterPanel: function () {
    if ($pnl_flter.not('.panel-collapsed'))
      $pnl_flter.addClass('panel-collapsed');
    $pnl_lst.closest('div.col').addClass('col-lg-12').removeClass('col-lg-9 col-md-8 col-sm-12')
    $pnl_flter.closest('div.col').hide('300');
  },
  applyFilter: function (ins) {
    var $li = $(ins).closest('li');
    $li.removeClass('active');
    this.pnl_tags.append(`<span class="tm-tag tm-tag-info" data-kat="`+$(ins).attr('data-kat')+`" data-id="`+$(ins).attr('data-id')+`">
                  <span>` + $li.find('span.caption').html() + `</span>
                  <a href="javascript:;" class="tm-tag-remove">x</a>
                </span>`);
    this.refreshFilter();
  },
  removeFilter: function (ins) {
    if($(ins).hasClass('tm-tag')){
      var $li =  $pnl_flter.find('a[data-kat="'+ins.attr('data-kat')+'"][data-id="'+ins.attr('data-id')+'"]').closest('li');
    }else var $li = $(ins).closest('li');
    $li.addClass('active');
    this.pnl_tags.find('span.tm-tag[data-kat="'+$(ins).attr('data-kat')+'"][data-id="'+$(ins).attr('data-id')+'"]').remove();
    if($(ins).attr('data-kat')=='search'){
      this.pnl_card.html('');
      this.get_act();
      $('input[name="search_act_keyword"]').val('');
    }else this.refreshFilter();
  },
  refreshFilter: function(target=null){
    var _this = this;
    if(target == null) var _trgt = _this.pnl_card;
    else var _trgt = _this.pnl_card.find('[data-offset="'+target+'"]');
    _trgt.find('.panel').hide();
    var isFilter = false;
    _trgt.find('.panel').hide();
    this.pnl_tags.find('.tm-tag').each(function(){
      if($(this).attr('data-kat')=='search') return true;
       _trgt.find('.panel.'+$(this).attr('data-kat')+'-'+$(this).attr('data-id')).show(300);
      isFilter = true;
    })
    if(!isFilter) this.pnl_card.find('.panel').show(300);
  },
  toogleLoadMore: function(){
    if(this.pnl_card.find('div[data-offset]:last').find('div.panel').length<__const.limit){
      $('a[data-aksi="card_laodmore"]').hide();
      $('a[data-aksi="card_alllaoded"]').show(300);
    }else{
      $('a[data-aksi="card_alllaoded"]').hide();
      $('a[data-aksi="card_laodmore"]').show(300);
    }
  },
  load_act : function(ins){
    $pnl_lst.hide();
    $pnl_flter.closest('div.col').hide();
    $pnl_act.show();
    $pnl_act.block();
    $.get(_page_info.url+'/kegiatan/'+$(ins).attr('data-id'),{ajax:true},function(respon){
      $pnl_act.find('#actdaftar_act_content').hide();
      $pnl_act.find('#actdaftar_act_content').html(respon);
      setTimeout(function(){
        act_layout.init();
        $pnl_act.unblock();
        $pnl_act.find('#actdaftar_act_content').show(300);
        $("html, body").animate({ scrollTop: 0 }, 600);
      },300);
    }).fail(function(){
      $pnl_act.notify('error',_page_info.title, _msg.unknow_error);
    })
  },
  back_to_list : function(){
    $pnl_act.hide();
    if($pnl_lst.closest('div.col').hasClass('col-lg-9')){ $pnl_flter.closest('div.col').show(300);};
    $pnl_lst.show(300);
  }
}
$('#btn_filter_act').click(function(){
  actdaftar.showFilterPanel();
})
$('#btn_search_act').click(function(){
  var key = $('input[name="search_act_keyword"]').val();
  var exist = actdaftar.pnl_tags.find('span[data-kat="search"]').attr('data-id');
  
  if(key == '' && typeof(exist) == 'undefined'){
    return false;
  }else{
    actdaftar.pnl_card.html('');
  }
  
  actdaftar.pnl_tags.find('span[data-kat="search"]').remove();
  if(key!==''){
    actdaftar.pnl_tags.append(`<span class="tm-tag tm-tag-warning" data-kat="search" data-id="`+key+`">
                  <span> pencarian : <b>` + key + `</b></span>
                  <a href="javascript:;" class="tm-tag-remove">x</a>
                </span>`);
  }
  actdaftar.get_act();
})
$('input[name="search_act_keyword"]').keypress(function(evt){
  if(evt.keyCode==13) $('#btn_search_act').click();
})
$('body').on('click','#actdaftar_filter_active a.tm-tag-remove',function(){
  actdaftar.removeFilter($(this).closest('span.tm-tag'));
})
$pnl_flter.on('click', 'a', function () {
  if ($(this).hasClass('panel-control-collapse')) {
    actdaftar.hideFilterPanel();
  } else if ($(this).hasClass('fltr_biaya')) {
    var $li = $(this).closest('li');
    if ($li.hasClass('active'))
      actdaftar.applyFilter($(this));
    else
      actdaftar.removeFilter($(this));
  }
})
$('a[data-aksi="card_laodmore"]').click(function(){
  actdaftar.get_act();
})
$('#actdaftar_panel_list').on('click','div.act-tile',function(){
  actdaftar.load_act($(this));
})
$('a[data-aksi="act_nav_back"]').click(function(){
  actdaftar.back_to_list();
})