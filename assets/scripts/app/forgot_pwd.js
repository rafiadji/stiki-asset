
var pagefrg = {
	form : null,
	panel : null,
	init : function(){
		this.form=$('form#forgot_pwd').my_plugins().form();
		this.panel=$('#panel-forgot_pwd').my_plugins().page();
	},
	validate : function (){
		var is=true;
		_email=this.form.find('input[name="email"]');
		if(_email.val()==null||_email.val()==''){
			_email.closest('.field').addClass('has-error').closest('.section').find('.alert span').html('Alamat email wajib diisi').closest('.alert').show(300);
			is=false;
		}else if(!is_validEmail(_email.val())){
			_email.closest('.field').addClass('has-error').closest('.section').find('.alert span').html('Alamat email tidak valid').closest('.alert').show(300);
			is=false;
		}else{
			_email.closest('.field').removeClass('has-error').closest('.section').find('.alert').hide(300);
		}
		return is;
	},
	request : function(){
		var _this=this;
		if(this.validate()){
			var _data=this.form.serialize();
			if($('.g-recaptcha').length) _data.recaptcha=$('#g-recaptcha-response').val();
			_this.alert('alert-wait');
			this.panel.block('.panel-footer');
			$.post(_page_info.url+'/verify',_data,function(respon){
				$('#alert-wait').hide();
				_this.panel.unblock();
				if(respon.isOk){
					localStorage.setItem(respon.token,respon.ls_token);
					$('section#content').html(respon.view);
				}else{
					_this.alert('alert-error',respon.msg)
				}
			},'json').fail(function(){
				_this.panel.unblock();
				_this.alert('alert-error',_msg.unknow_error);
			})
		}
	},
	alert : function(show,msg=null){
		$('#alert-dir').hide();
		$('#alert-error').hide();
		$('#alert-wait').hide();
		$('#alert-dir-captcha').hide();
		if(msg!=null) $('#'+show).find('span').html(msg);
		$('#'+show).show(300);
	}
}
$(document).ready(function(){
	pagefrg.init();
})

function on_captcha(val){
	if($('.g-recaptcha').length && val!=null && val!=''){
		$('#section-email').show(300);
		pagefrg.alert('alert-dir');
	}else{
		$('#section-email').hide(300);
		pagefrg.alert('alert-dir-captcha');
	} 
}

$('label[data-aksi="reset"]').click(function(){
	pagefrg.request();
})