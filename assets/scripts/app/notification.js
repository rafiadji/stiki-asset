$.fn.notification = function(set_config) 
{
	return this.each(function()
	{
		var e = $(this);
		var id;
		var selector;
		var unread_count = 0;
		var read_count = 0;
		var total_count = 0;
		var html = '';
		var item_selector = 'li.my-notif-item';

		var config = $.extend({
			get_url: e.attr('data-get-url'),
			read_url: e.attr('data-read-url'),
			interval: false, // in milisecond
		}, set_config);
		
		// method
		e.initialize = function()
		{
			e.id = Math.floor((Math.random() * 10000) + 1);
			e.selector = '[mynotif-id="'+e.id+'"]';
			e.attr('mynotif-id', e.id);
			this.getNotif();
			
			if(config.interval) setInterval(function(){ e.getNotif(); }, config.interval);			
		}
		
		e.getNotif = function()
		{
			$.ajax({
				url: config.get_url,
				method: 'GET',
				dataType: 'JSON',
				success: function(data)
				{
					html = '';
					$.each(data, function(key, row){
						total_count++;
						if(row.is_unread == '1') unread_count++;
						else read_count++;
						html += '<li class="media my-notif-item '+(row.is_unread == '1' ? 'unread' : '' )+'" data-id="'+row.id_notif+'" >';
						html += '	<div class="media-body">';
            html += '    <h5 class="media-heading">'+row.sender_name+'';
            html += '      <small class="text-muted">- '+row.formated_date_sent+'</small>';
            html += '    </h5> ';
						html += '		'+row.content;
            html += '  </div>';
            html += '</li>';
					})
					if(!data.length)
					{
						html += '<li class="media my-notif-blank">';
						html += '	<div class="media-body">';
            html += '    <h5 class="media-heading"><small class="text-muted">Tidak ada pemberitahuan baru</small></h5> ';
            html += '  </div>';
            html += '</li>';
					}
					$(item_selector+', .my-notif-blank', e).remove();
					e.append(html);
					$(item_selector, e).css('cursor', 'pointer');
					$(item_selector+'.unread', e).css('border-left','5px solid red');
					e.calculate();
					
				},
				error: function(error){
				}
			})
		}
		
		e.readNotif = function(id)
		{
			var id = id;
			$.ajax({
				url: config.read_url,
				method: 'POST',
				data:{id: id},
				dataType: 'JSON',
				success: function(res)
				{
					if(res.success)
					{
						$(item_selector+'[data-id="'+id+'"]', e).removeClass('unread').css('border-left', '5px solid #ddd');
						e.calculate();
						if(res.data.target_url) window.location = res.data.target_url;
					}
				},
				error: function(error){
				}
			})
		}
		
		e.calculate = function()
		{
			total_count = 0;
			unread_count = 0;
			read_count = 0;
			$('.my-notif-total-badge, .my-notif-unread-badge, .my-notif-read-badge').hide();
			$(item_selector, e).each(function()
			{
				total_count++;
				if($(this).hasClass('unread')) unread_count++;
				else read_count++;
			})
			$('.my-notif-unread-count, .my-notif-unread-badge').text(unread_count);
			$('.my-notif-read-count, .my-notif-read-badge').text(read_count);
			$('.my-notif-total-count, .my-notif-total-badge').text(total_count);
			if(total_count > 0) $('.my-notif-total-badge').show();
			if(unread_count > 0) $('.my-notif-unread-badge').show();
			if(read_count > 0) $('.my-notif-read-badge').show();
		}
		
		// event method
		e.on('click', item_selector, function(){
			id = $(this).data('id');
			e.readNotif(id);
		})
		
		
		e.initialize();
	})
}

