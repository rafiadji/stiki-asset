var admin={
	handle_sidemenu:function($elm=null,rec=0){
		if(rec>10) return false;
		if($elm==null){
			$elm=$('#admin-menu').find('[href="'+_page_info.url+'"]').closest('li');
			$elm.addClass('active');
			this.handle_sidemenu($elm,1);
		}
		else{
			$_elm=$elm.closest('ul').closest('li').find('.accordion-toggle');
			if($_elm.length>0){
				$_elm.addClass('menu-open');
				if($_elm.closest('li').closest('ul').hasClass('sidebar-menu')) return false;
				else this.handle_sidemenu($_elm.closest('li'),(rec+1));
			}
		}
	}
}
$(document).ready(function(){
	Core.init();
	admin.handle_sidemenu();
	
//	$('.my-notification').notification({
//		get_url: _site_url+'/dashboard/get_notif_ajax',
//		read_url: _site_url+'/dashboard/read_notif_ajax',
//		interval: 5000,
//	});
})