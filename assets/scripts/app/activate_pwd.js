
$('button[data-aksi="simpan"]').click(function(){
	pagesetpwd.simpan();
})
var pagesetpwd={
	form : null,
	panel : null,
	pwd : null,
	init : function(){
		this.form=$('form#setpwd').my_plugins().form();
		this.pwd=this.form.find('input[name="password"]');
		this.panel=$('#panel-setpwd').my_plugins().page();
	},
	reset_validate : function(){
		this.form.find('.has-error').removeClass('has-error');
		this.form.find('.field alert').hide();
	},
	validate : function(){
		var is=true;
		this.reset_validate();
		_pwd=this.pwd;
		_repwd=this.form.find('input[name="repassword"]');
		if(_pwd.val()==null||_pwd.val()==''){
			_pwd.closest('.field').addClass('has-error').find('.alert span').html('Password wajib diisi').closest('.alert').show(300);
			is=false;
		}else{
			_pwd.closest('.field').removeClass('has-error').find('.alert').hide(300);
		}
		
		if(_repwd.val()==null||_repwd.val()==''){
			_repwd.closest('.field').addClass('has-error').find('.alert span').html('Ulangi password wajib diisi').closest('.alert').show(300);
			is=false;
			console.log(_repwd.val());
		}else if(_repwd.val()!=_pwd.val()){
			_repwd.closest('.field').addClass('has-error').find('.alert span').html('Password tidak sama').closest('.alert').show(300);
			is=false;
		}else{
			_repwd.closest('.field').removeClass('has-error').find('.alert').hide(300);
		}
		return is;
	},
	simpan : function(){
		var _this=this;
		if(this.validate()){
			_this.panel.block();
			$.post(_page_info.url+'/setactive',{token:btoa(btoa(_this.pwd.val()))},function(respon){
				if(respon.isOk){
					setTimeout(function() {
				      	$('#alert-error').hide(300);
						$('section#content').html(respon.html);
				    }, 2000)
				}else{
					_this.panel.unblock();
					$('#alert-error').find('span').html(respon.msg).closest('.alert').show(300);
				}
			},'json').fail(function(){
				_this.panel.notify('error',_page_info.title,_msg.unknow_error);
			})
		}
	}
}
$(document).ready(function(){
	pagesetpwd.init();
})