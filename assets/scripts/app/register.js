$(document).ready(function(){
	"use strict";  
    Core.init();
    regpage.init();
 });
$('button[data-aksi="daftar"]').click(function(){
	regpage.register();
})
$('body').on('click','a[data-aksi="resend_activation"]', function(){
	regpage.resend_activation();
})
$('[data-for="hover"]').mouseenter(function(){
	$('span[name="_terms"]').popover('show');
})
$('[data-for="hover"]').mouseleave(function(){
	$('span[name="_terms"]').popover('hide');
})
$('input[name="terms"]').change(function(){
	if($(this).is(':checked')){
		$('[data-for="hover"]').hide();
		$('[data-aksi="daftar"]').show();
	}else{
		$('[data-for="hover"]').show();
		$('[data-aksi="daftar"]').hide();
	}
})
function on_captcha(val){
	if($('.g-recaptcha').length && val!=null && val!=''){
		regpage.form.find('.panel-footer').show(300);
	}else regpage.form.find('.panel-footer').hide(300);
}
var regpage={
	form : null,
	panel : null,
	init : function(){
		this.form=$('form#register').my_plugins().form();
		this.panel=$('#reg-panel').my_plugins().page();
		$('span[name="_terms"]').popover({placement:'bottom',trigger:'hover focus',content:'Cetang untuk melanjutkan'});
		this.reset();
	},
	register : function (){
		var _this=this;
		if(this.validate()){
			var _data=this.form.serialize();
			if($('.g-recaptcha').length) _data.recaptcha=$('#g-recaptcha-response').val();
			$('#alert-wait').show(300);
			this.panel.block();
			$.post(_page_info.url+'/do_register',_data,function(respon){
				$('#alert-wait').hide();
				_this.panel.unblock();
				if(respon.isOk){
					localStorage.setItem(respon.token,respon.ls_token);
					$('section#content').html(respon.view);
				}else{
					$('#alert-error').find('span').html(respon.msg).closest('.alert').show(300);
				}
			},'json').fail(function(){
				_this.panel.unblock();
				_this.panel.notify('error',_page_info.title,_msg.unknow_error);
			})
		}
	},
	reset : function(){
		this.reset_validate();
		$('[data-for="hover"]').show();
		$('[data-aksi="daftar"]').hide();
		$('.alert').hide();
	},
	reset_validate : function(){
		this.form.find('.has-error').removeClass('has-error');
		this.form.find('.field alert').hide();
	},
	validate : function(){
		var is=true;
		this.reset_validate();
		_nama=this.form.find('input[name="nama"]');
		_email=this.form.find('input[name="email"]');
		if(_nama.val()==null||_nama.val()==''){
			_nama.closest('.field').addClass('has-error').find('.alert span').html('Nama lengkap wajib diisi').closest('.alert').show(300);
			is=false;
		}else{
			_nama.closest('.field').removeClass('has-error').find('.alert').hide(300);
		}
		
		if(_email.val()==null||_email.val()==''){
			_email.closest('.field').addClass('has-error').find('.alert span').html('Alamat email wajib diisi').closest('.alert').show(300);
			is=false;
		}else if(!is_validEmail(_email.val())){
			_email.closest('.field').addClass('has-error').find('.alert span').html('Alamat email tidak valid').closest('.alert').show(300);
			is=false;
		}else{
			_email.closest('.field').removeClass('has-error').find('.alert').hide(300);
		}
		return is;
	},
	resend_activation : function(){
		var _this=this;
		if(this.validate()){
			var _data=this.form.serialize();
			$('#alert-wait').show(300);
			this.panel.block();
			$.post(_page_info.url+'/resend_activation',_data,function(respon){
				$('#alert-wait').hide();
				_this.panel.unblock();
				if(respon.isOk){
					localStorage.setItem(respon.token,respon.ls_token);
					$('section#content').html(respon.view);
				}else{
					$('#alert-error').find('span').html(respon.msg).closest('.alert').show(300);
				}
			},'json').fail(function(){
				_this.panel.unblock();
				_this.panel.notify('error',_page_info.title,_msg.unknow_error);
			})
		}
	}
};
