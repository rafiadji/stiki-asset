var hbh_jenis={
	dt : null,
	frm : null,
	mdl_inp : $('#mdl_inp').my_plugins().page(),
	mdl_prf : $('#mdl_profile').my_plugins().page(),
	
	open_dialog_input : function(mode,callback=null,par=null){
		var _this=this;
		this.mdl_inp.mdl_open({title:'Tambah Jenis Hibah',clearbody:false});
		if(_this.frm==null){
			$.post(_page_info.url+'/get_frominput',function(respon){
				_this.mdl_inp.find('.modal-body').html(respon);
				_this.frm=$('form#hbh-jenis').my_plugins().form();
				
				if(callback=='open_ubah') _this.open_ubah(par);
				_this.mdl_inp.enable_btn();
			}).fail(function(){
				_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
			})
		}else{
			_this.frm.reset();
			$(_this.frm.selector).find('[name="isAktif"]').bootstrapToggle('on').trigger('change');

			_this.mdl_inp.unblock();
			_this.mdl_inp.enable_btn();
		}
	},
	open_ubah : function(par){
		var _this=this;
		if(this.frm==null){
			this.open_dialog_input('ubah','open_ubah',par);
		}else{
			this.mdl_inp.mdl_open({title:'Ubah Jenis Hibah '+par.nama_jenis,clearbody:false});
			$.post(_page_info.url+'/get_ubahdata',{id:par.id},function(respon){
				_this.frm.reset();

				_this.frm.setdata(respon);
				_this.mdl_inp.unblock();
				_this.mdl_inp.enable_btn();	
			},'json').fail(function(){
				_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
			})
		}
	},
	simpan : function(){
		var _this=this;
		if(this.frm.validate()){
			this.mdl_inp.block();
			this.mdl_inp.disable_btn();
		 	var data={data:this.frm.serialize()}
		 	$.post(_page_info.url+'/set_simpan',data,function(respon){
		 		if(respon.isOk){
		 			_this.mdl_inp.notify('success',_page_info.title,'Jenis hibah baru berhasil ditambahkan');
		 			_this.mdl_inp.mdl_hide();
		 			_this.dt.reload();
		 		}else{
					_this.mdl_inp.notify('error',_page_info.title,respon.msg);		 			
		 		}
		 		_this.mdl_inp.unblock();
		 		_this.mdl_inp.enable_btn();
		 	},'json').fail(function(){
		 		_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		 		_this.mdl_inp.unblock();
		 		_this.mdl_inp.enable_btn();
		 	})
		}
	},
	hapus : function(id){
		var _this=this;
		$.post(_page_info.url+'/set_hapus',{id:id},function(respon){
			if(respon.isOk){
				_this.mdl_inp.notify('success',_page_info.title,'Pengguna berhasil dihapus.');	
				_this.dt.reload();
			}else{
				_this.mdl_inp.notify('error',_page_info.title,respon.msg);
			}
		},'json').fail(function(){
			_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		})
	},
	set_aktiftoggle_change : function(elm){
		var _this=this;
		var par=JSON.parse(urldecode($(elm).attr('data-par')));
		if($(elm).is(':checked')) var isCheck='YES'; else var isCheck='NO';
		$.post(_page_info.url+'/set_aktifchange',{id:par.id,aktif:isCheck},function(respon){
			if(respon.isOk){
				_this.mdl_inp.notify('success',_page_info.title,'Berhasil merubah status keaktifan akun pengguna.');
			}else{
				_this.mdl_inp.notify('error',_page_info.title,respon.msg);
			}
		},'json').fail(function(){
			_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		})	
	}
}
$(document).ready(function(){
	hbh_jenis.dt=$('#main_dt').my_plugins().datatable({url:'/hibah_jenis/getdata'},['id']);
})
$('button[data-aksi="tambah"]').click(function(){
	hbh_jenis.open_dialog_input('Tambah');
})
hbh_jenis.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
	hbh_jenis.simpan();
})
$('body').on('click','a[data-aksi="adm-del"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	bootbox.confirm('Apakah anda yakin akan menghapus jenis hibah <b>'+par.nama_jenis+'</b> ?',function(respon){
		if(respon) hbh_jenis.hapus(par.id);
	})
})
$('body').on('change','input[name="jenis_isaktif"]',function(){
	hbh_jenis.set_aktiftoggle_change($(this));
})
$('body').on('click','a[data-aksi="adm-ubah"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	hbh_jenis.open_ubah(par);
})