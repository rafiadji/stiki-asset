var hbh_brab={
	dt : null,
	tb_inp : 'table#tb_rab',
	mdl_inp : $('#mdl_inp').my_plugins().page(),
	mdl_det : $('#mdl_det').my_plugins().page(),
  rab_itv : null,
  acc_itv : null,
  rab_itv_tm : 3000,
  rab_prp_id : null,
  rab_prp_nama : null,
	open_rab : function(par=null){
		var _this=this;
    
    if(par==null) par = {id:_this.rab_prp_id,nama:_this.rab_prp_nama};
    
		this.mdl_inp.mdl_open({title:'Pembahasan Proposal '+par.nama,clearbody:false});
    _this.mdl_inp.find('button[data-aksi="simpan"]').hide();
		$.post(_page_info.url+'/get_formrab',par,function(respon){
      _this.rab_prp_id = par.id;
      _this.rab_prp_nama = par.nama;
      _this.mdl_inp.find('.modal-body').html(respon);
      if(_this.mdl_inp.find('a[data-aksi="bahasrab_mulai"]').length > 0){
        
      }else{
        _this.mdl_inp.find('input[name="anggaran_disetujui"]').each(function(){
          _this.tbnip_currency_masking($(this));
          _this.mdl_inp.find('button[data-aksi="simpan"]').show();
        })
        _this.tbnip_caltot();
        if(_this.mdl_inp.find('.rab_keepupdate').length>0)
          _this.rab_itv =  setInterval(function(){hbh_brab.get_rab_realtime()},hbh_brab.rab_itv_tm);
        if(_this.mdl_inp.find('.acc_keepupdate').length>0)
          _this.acc_itv =  setInterval(function(){hbh_brab.get_acc_realtime()},hbh_brab.rab_itv_tm);
      }
      _this.mdl_inp.enable_btn();
    }).fail(function(){
      _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
    })
	},
  set_mulaibahas : function(){
    var _this = this;
    var par = {id:_this.rab_prp_id};
    this.mdl_inp.block();
    this.mdl_inp.disable_btn();
    $.post(_page_info.url+'/set_mulaibahas',par,function(respon){
      if(respon.isOk){
        _this.open_rab();
        _this.dt.reload();
      }
      _this.mdl_inp.unblock();
      _this.mdl_inp.enable_btn();
    },'json').fail(function(){
      _this.mdl_inp.unblock();
      _this.mdl_inp.enable_btn();
    });
  },
  get_rab_realtime : function(){
    var _this = this;
    $.post(_page_info.url+'/get_rabdata',{id:_this.rab_prp_id},function(respon){
      $.each(respon.rab,function(i,val){
        var $td = _this.mdl_inp.find('#tb_rab tr[data-id="'+val.id+'"]').find('.rab_keepupdate');
        $td.attr('data-value',val.anggaran_disetujui);
        $td.html(number_to_currency(val.anggaran_disetujui));

        var $td = _this.mdl_inp.find('#tb_rab tr[data-id="'+val.id+'"]').find('.ket_keepupdate');
        $td.html(val.keterangan);
      })
      _this.tbnip_caltot();
      _this.mdl_inp.find('[data-id="last-changed"]').html(respon.lastChanged);
    },'json').fail(function(){
      _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
    })
  },
  get_acc_realtime : function(){
    var _this = this;
    $.post(_page_info.url+'/get_rabAccData',{id:_this.rab_prp_id},function(respon){
      _this.mdl_inp.find('.acc_keepupdate').html(respon);
      var $is = 0;
      var $tot = 0;
      _this.mdl_inp.find('.acc_keepupdate').find('table tbody tr').each(function(){
        $tot=$tot+1;
        if($(this).find('td:eq(2)').html()=='Setuju') $is=$is+1;
      })
      _this.mdl_inp.find('[data-id="title_acc_keepupdate"]').html( Math.round($is/$tot*100)+'% ('+$is+'/'+$tot+')');
      if($is==$tot) _this.mdl_inp.find('[data-id="title_acc_keepupdate"]').removeClass('label-warning').addClass('label-success');
      else _this.mdl_inp.find('[data-id="title_acc_keepupdate"]').removeClass('label-success').addClass('label-warning');
    }).fail(function(){
      _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
    })
  },
  tbnip_currency_masking : function($ins){
    $ins.attr('data-value',$ins.val());
    $ins.val(number_to_currency($ins.val()));
  },
	tbnip_serialize : function(){
		var data = [];
		$(this.tb_inp).find('tbody tr').each(function(){
			var _data = {};
			var id = $(this).attr('data-id');
			if(typeof id === 'undefined') id=null;
			_data['id'] = id;
			$(this).find('input').each(function(){
				if(typeof $(this).attr('data-value')==='undefined')
				 	_data[$(this).attr('name')] = $(this).val();
				else _data[$(this).attr('name')] = $(this).attr('data-value');
			})
			data.push(_data);
		})
		return data;
	},
	tbnip_caltot : function(){
		var tot = 0;
		$(this.tb_inp).find('tbody tr').each(function(){
			var subtot = parseInt($(this).find('[name="anggaran_disetujui"]').attr('data-value'));
			if(subtot != 'NaN' && subtot>0) tot=tot+subtot;
		})
		$(this.tb_inp).find('tfoot #rab_tot').html(number_to_currency(tot));
	},
	set_selesaibahas : function(){
		var _this=this;
    this.mdl_inp.block();
    this.mdl_inp.disable_btn();
		$.post(_page_info.url+'/set_selesaibahas',{id:_this.rab_prp_id},function(respon){
      if(respon.isOk){
        _this.dt.reload();
        _this.open_rab();
      }else{
        _this.mdl_inp.notify('error',_page_info.title,respon.msg);		 			
      }
      $('body').addClass('modal-open');
      _this.mdl_inp.unblock();
      _this.mdl_inp.enable_btn();
    },'json').fail(function(){
      _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
      _this.mdl_inp.unblock();
      _this.mdl_inp.enable_btn();
    })
	},
  set_accbahas : function(){
    var _this=this;
    this.mdl_inp.block();
    this.mdl_inp.disable_btn();
		$.post(_page_info.url+'/set_accbahas',{id:_this.rab_prp_id},function(respon){
      if(respon.isOk){
        _this.dt.reload();
        _this.open_rab();
      }else{
        _this.mdl_inp.notify('error',_page_info.title,respon.msg);		 			
      }
      _this.mdl_inp.unblock();
      _this.mdl_inp.enable_btn();
    },'json').fail(function(){
      _this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
      _this.mdl_inp.unblock();
      _this.mdl_inp.enable_btn();
    })
  },
	open_lihat : function(par){
		var _this=this;
		this.mdl_det.mdl_open({title:'Lihat Proposal '+par.nama,clearbody:false});
		
		$.post(_page_info.url+'/get_lihat',{id:par.id},function(respon){
			_this.mdl_det.find('.modal-body').html(respon);
			_this.mdl_det.enable_btn();
		}).fail(function(){
			_this.mdl_det.notify('error',_page_info.title,_msg.unknow_error);
		})
	},
  set_rabasdemand : function($ins){
    var $tr = $ins.closest('tr');
    $tr.find('input[name="anggaran_disetujui"]').val($tr.find('td[data-id="jumlah-minta"]').attr('data-value')).trigger('blur');
  },
  set_draftrab : function($ins){
    var _this = this;
    var $tr = $ins.closest('tr');
    var data = {
      id:$tr.attr('data-id'),
      anggaran_disetujui:$tr.find('[name="anggaran_disetujui"]').attr('data-value'),
      keterangan:$tr.find('[name="keterangan"]').val()
    }
    $.post(_page_info.url+'/set_draftrab',data,function(respon){
			if(respon.isOk){
        _this.mdl_inp.find('i[data-id="last-changed"]').html(respon.data);
        _this.dt.reload();
      }else{
        _this.mdl_inp.notify('error',_page_info.title,respon.msg);
      }
		},'json').fail(function(){
			_this.mdl_det.notify('error',_page_info.title,_msg.unknow_error);
		})
  }
}
$(document).ready(function(){
	hbh_brab.dt=$('#main_dt').my_plugins().datatable({url:'/hibah_pembahasan_rab/getdata'},['id']);
})
hbh_brab.mdl_inp.find('button[data-dismiss="modal"]').click(function(){
  clearInterval(hbh_brab.rab_itv);
  clearInterval(hbh_brab.acc_itv);
})
hbh_brab.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
  var title = $(this).closest('.modal').find('.modal-title').html();
	bootbox.confirm('Apakah anda yakin menyatakan <b>'+title+'</b> telah selesai ?',function(respon){
		if(respon) hbh_brab.set_selesaibahas();
    else $('body').addClass('modal-open');
    
    $('body').addClass('modal-open');
	})
})
$('body').on('click','a[data-aksi="adm-rab"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	hbh_brab.open_rab(par);
})
$('body').on('click','a[data-aksi="bahasrab_mulai"]',function(){
	hbh_brab.set_mulaibahas();
})
$('body').on('focus','input[name="anggaran_disetujui"]',function(){
	$(this).val($(this).attr('data-value'));
})
$('body').on('blur','input[name="anggaran_disetujui"]',function(){
	hbh_brab.tbnip_currency_masking($(this));
  hbh_brab.set_draftrab($(this));
  hbh_brab.tbnip_caltot();
})
$('body').on('blur','textarea[name="keterangan"]',function(){
  hbh_brab.set_draftrab($(this));
})
$('body').on('keyup','input[name="harga_satuan"],input[name="jumlah"]',function(){
	$(this).attr('data-value',$(this).val());
	hbh_prop.tbnip_calsubtot($(this));
})
$('body').on('click','a.rab_diterimaasdiajukan',function(){
	hbh_brab.set_rabasdemand($(this));
})
$('body').on('click','a[data-aksi="adm-lihat"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	hbh_brab.open_lihat(par);
})
$('body').on('click','[data-aksi="bahasrab_acc"]',function(){
  hbh_brab.set_accbahas();
})