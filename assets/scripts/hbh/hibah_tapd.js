var hbh_tapd={
	dt : null,
	frm : null,
	tb_inp : 'table#tb_tim_member',
	mdl_inp : $('#mdl_inp').my_plugins().page(),
	mdl_det : $('#mdl_det').my_plugins().page(),
	
	open_dialog_input : function(mode,callback=null,par=null){
		var _this=this;
		this.mdl_inp.mdl_open({title:'Tambah Tim Anggaran Pemerintah Daerah',clearbody:false});
		if(_this.frm==null){
			$.post(_page_info.url+'/get_frominput',function(respon){
				_this.mdl_inp.find('.modal-body').html(respon);
				_this.frm=$('form#hbh-jenis').my_plugins().form();
				
				_this.tbinp_init();

				if(callback=='open_ubah') _this.open_ubah(par);
				_this.mdl_inp.enable_btn();
			}).fail(function(){
				_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
			})
		}else{
			_this.frm.reset();
			$(_this.frm.selector).find('[name="isAktif"]').bootstrapToggle('on').trigger('change');

			_this.tbinp_reset();
			_this.tbinp_init();

			_this.mdl_inp.unblock();
			_this.mdl_inp.enable_btn();
		}
	},
	tbinp_init : function(){
		$(this.tb_inp).find('select:not(.inited)').each(function(){
			var uid = uniqid().trim();
			$(this).attr('data-id',uid);
			$('select[name="'+$(this).attr('name')+'"][data-id="'+$(this).attr('data-id')+'"]').my_plugins().select2();
			$(this).addClass('inited');
		})
	},
	tbinp_reset : function(addrow = true){
		$(this.tb_inp).find('tbody tr').remove();
		if(addrow) this.tbinp_addrow();
	},
	tbinp_addrow : function(data = null){
		var _this = this;
		$(this.tb_inp).find('tbody').append(`<tr>
				<td></td>
				<td><select name="user_username" class="form-control">`+opt.usr+`</select></td>
				<td><input type="text" name="latar_belakang" class="form-control"></td>
				<td><select name="kategori" class="form-control">`+opt.kat+`</select></td>
				<td><a href="javascript:;" class="del-row"><i class="fa fa-trash-o icon20"></i></a></td>
			</tr>`);
		if(data != null){
			$.each(data,function(i,val){
				$(_this.tb_inp).find('tbody tr:last').find('[name="'+i+'"]').val(val);
			})
		}
		this.tbinp_urutkan();
		this.tbinp_init();
	},
	tbinp_removerow : function($ins){
		$ins.closest('tr').remove();
		this.tbinp_urutkan();
	},
	tbinp_urutkan : function(){
		$(this.tb_inp).find('tbody tr').each(function(i){
			$(this).find('td:first').html(i+1);
		})
	},
	tbinp_validate : function($cnt){
		var val = $cnt.val();
		var _this = this;
		$(this.tb_inp).find('select[name="user_username"]').each(function(){
			if(!$(this).is($cnt) && val===$(this).val()){
				$cnt.val(null).trigger('change.select2');
				_this.mdl_inp.notify('error',_page_info.title,"Anggota yang sama sudah terdaftar pada tabel");
				return false;
			}
		});
	},
	tbnip_serialize : function(){
		var data = [];
		$(this.tb_inp).find('tbody tr').each(function(){
			var _data = {};
			var req = $(this).find('select[name="user_username"]').val(); 
			if(req!=null && req.length>0){
				$(this).find('select,input').each(function(){
					 _data[$(this).attr('name')] = $(this).val();
				})
				data.push(_data);	
			}
			
		})
		return data;
	},
	tbnip_restore : function(data){
		var _this = this;
		$.each(data, function(i,dt){
			_this.tbinp_addrow(dt);
		})
	},
	open_ubah : function(par){
		var _this=this;
		if(this.frm==null){
			this.open_dialog_input('ubah','open_ubah',par);
		}else{
			this.mdl_inp.mdl_open({title:'Ubah Tim Anggaran Pemerintah Daerah '+par.nama,clearbody:false});
			$.post(_page_info.url+'/get_ubahdata',{id:par.id},function(respon){
				_this.frm.reset();
				_this.tbinp_reset(false);

				_this.frm.setdata(respon.tim);
				_this.tbnip_restore(respon.member);
				_this.mdl_inp.unblock();
				_this.mdl_inp.enable_btn();	
			},'json').fail(function(){
				_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
			})
		}
	},
	simpan : function(){
		var _this=this;
		if(this.frm.validate()){
			this.mdl_inp.block();
			this.mdl_inp.disable_btn();
		 	var data={data:this.frm.serialize(),member:this.tbnip_serialize()}
		 	$.post(_page_info.url+'/set_simpan',data,function(respon){
		 		if(respon.isOk){
		 			_this.mdl_inp.notify('success',_page_info.title,'Tim APD baru berhasil ditambahkan');
		 			_this.mdl_inp.mdl_hide();
		 			_this.dt.reload();
		 		}else{
					_this.mdl_inp.notify('error',_page_info.title,respon.msg);		 			
		 		}
		 		_this.mdl_inp.unblock();
		 		_this.mdl_inp.enable_btn();
		 	},'json').fail(function(){
		 		_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		 		_this.mdl_inp.unblock();
		 		_this.mdl_inp.enable_btn();
		 	})
		}
	},
	hapus : function(id){
		var _this=this;
		$.post(_page_info.url+'/set_hapus',{id:id},function(respon){
			if(respon.isOk){
				_this.mdl_inp.notify('success',_page_info.title,'Tim APD berhasil dihapus.');	
				_this.dt.reload();
			}else{
				_this.mdl_inp.notify('error',_page_info.title,respon.msg);
			}
		},'json').fail(function(){
			_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		})
	},
	set_aktiftoggle_change : function(elm){
		var _this=this;
		var par=JSON.parse(urldecode($(elm).attr('data-par')));
		if($(elm).is(':checked')) var isCheck='YES'; else var isCheck='NO';
		$.post(_page_info.url+'/set_aktifchange',{id:par.id,aktif:isCheck},function(respon){
			if(respon.isOk){
				_this.mdl_inp.notify('success',_page_info.title,'Berhasil merubah status keaktifan Tim Anggaran Pemerintah Daerah.');
			}else{
				_this.mdl_inp.notify('error',_page_info.title,respon.msg);
			}
		},'json').fail(function(){
			_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		})	
	},
	open_lihat : function(par){
		var _this=this;
		this.mdl_det.mdl_open({title:'Lihat Tim Anggaran Pemerintah Daerah '+par.nama,clearbody:false});
		
		$.post(_page_info.url+'/get_lihat',{id:par.id},function(respon){
			_this.mdl_det.find('.modal-body').html(respon);
			_this.mdl_det.enable_btn();
		}).fail(function(){
			_this.mdl_det.notify('error',_page_info.title,_msg.unknow_error);
		})
	},
	open_eval : function(par){
		var _this=this;
		this.mdl_det.mdl_open({title:'Lihat Hasil Evaluasi TAPD '+par.nama,clearbody:false});
		
		$.post(_page_info.url+'/get_lihateval',{id:par.id},function(respon){
			_this.mdl_det.find('.modal-body').html(respon);
			_this.mdl_det.enable_btn();
		}).fail(function(){
			_this.mdl_det.notify('error',_page_info.title,_msg.unknow_error);
		})
	}
}
$(document).ready(function(){
	hbh_tapd.dt=$('#main_dt').my_plugins().datatable({url:'/hibah_tapd/getdata'},['id']);
})
$('button[data-aksi="tambah"]').click(function(){
	hbh_tapd.open_dialog_input('Tambah');
})
hbh_tapd.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
	hbh_tapd.simpan();
})
$('body').on('click','a[data-aksi="adm-del"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	bootbox.confirm('Apakah anda yakin akan menghapus Tim Anggaran Pemerintah Daerah <b>'+par.nama+'</b> ?',function(respon){
		if(respon) hbh_tapd.hapus(par.id);
	})
})
$('body').on('change','input[name="tim_isaktif"]',function(){
	hbh_tapd.set_aktiftoggle_change($(this));
})
$('body').on('click','a[data-aksi="adm-ubah"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	hbh_tapd.open_ubah(par);
})
$('body').on('click','a[data-aksi="adm-lihat"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	hbh_tapd.open_lihat(par);
})
$('body').on('click','a[data-aksi="adm-eval"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	hbh_tapd.open_eval(par);
})
$('body').on('click','a[data-aksi="tambah-anggota"]',function(){
	hbh_tapd.tbinp_addrow();
})
$('body').on('click','a.del-row',function(){
	hbh_tapd.tbinp_removerow($(this));
})
$('body').on('change',hbh_tapd.tb_inp+' select',function(){
	hbh_tapd.tbinp_validate($(this));
})