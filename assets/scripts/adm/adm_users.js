var adm_users={
	dt : null,
	frm_bio : null,
	frm_usr : null,
	mdl_inp : $('#mdl_inp').my_plugins().page(),
	mdl_prf : $('#mdl_profile').my_plugins().page(),
	open_profile : function(par){
		var _this=this;
		this.mdl_prf.mdl_open({title:'Profile Pengguna '+par.nama,clearbody:false});
		$.post(_page_info.url+'/get_profile',{id:par.id},function(respon){
			_this.mdl_prf.find('.modal-body').html(respon);
			_this.mdl_prf.enable_btn();
		}).fail(function(){
			_this.mdl_prf.notify('error',_page_info.title,_msg.unknow_error);
		})
	},
	open_dialog_input : function(mode,callback=null,par=null){
		var _this=this;
		this.mdl_inp.mdl_open({title:'Tambah Pengguna',clearbody:false});
		if(_this.frm_bio==null){
			$.post(_page_info.url+'/get_frominput',function(respon){
				_this.mdl_inp.find('.modal-body').html(respon);
				_this.frm_bio=$('form#adm-users-bio').my_plugins().form();
				_this.frm_usr=$('form#adm-users-usr').my_plugins().form();
				
				if(callback=='open_ubah') _this.open_ubah(par);
				_this.mdl_inp.enable_btn();
			}).fail(function(){
				_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
			})
		}else{
			_this.frm_bio.reset();
			_this.frm_usr.reset();
			$(_this.frm_usr.selector).find('[name="isAktif"]').bootstrapToggle('on').trigger('change');

			_this.mdl_inp.unblock();
			_this.mdl_inp.enable_btn();
		}
	},
	open_ubah : function(par){
		var _this=this;
		if(this.frm_bio==null){
			this.open_dialog_input('ubah','open_ubah',par);
		}else{
			this.mdl_inp.mdl_open({title:'Ubah Pengguna '+par.nama,clearbody:false});
			$.post(_page_info.url+'/get_ubahdata',{id:par.id},function(respon){
				_this.frm_bio.reset();
				_this.frm_usr.reset();

				_this.frm_bio.setdata(respon);
				_this.frm_usr.setdata(respon);
				$(_this.frm_usr.selector).find('[name="isAktif"]').trigger('change');

				_this.mdl_inp.unblock();
				_this.mdl_inp.enable_btn();	
			},'json').fail(function(){
				_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
			})
		}
	},
	simpan : function(){
		var _this=this;
		if(this.frm_bio.validate() && this.frm_usr.validate() && this.is_pwd_cnfrm()){
			this.mdl_inp.block();
			this.mdl_inp.disable_btn();
		 	var data={peg:this.frm_bio.serialize(),usr:this.frm_usr.serialize()}
		 	$.post(_page_info.url+'/set_simpan',data,function(respon){
		 		if(respon.isOk){
		 			_this.mdl_inp.notify('success',_page_info.title,'Pengguna baru berhasil ditambahkan');
		 			_this.mdl_inp.mdl_hide();
		 			_this.dt.reload();
		 		}else{
					_this.mdl_inp.notify('error',_page_info.title,respon.msg);		 			
		 		}
		 		_this.mdl_inp.unblock();
		 		_this.mdl_inp.enable_btn();
		 	},'json').fail(function(){
		 		_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		 		_this.mdl_inp.unblock();
		 		_this.mdl_inp.enable_btn();
		 	})
		}
	},
	is_pwd_cnfrm : function(){
		var pwd=$('input[name="user_password"]');
		var repwd=$('input[name="re_password"]');
		if(pwd.val()!=repwd.val()){
			pwd.closest('.form-group').addClass('has-error');
			repwd.closest('.form-group').addClass('has-error');
			repwd.closest('.form-group').find('.col-inp').append('<label class="hlp-msg">Password tidak sama</label>');  
			return false;
		}else{
			pwd.closest('.form-group').removeClass('has-error').addClass('has-success');
			repwd.closest('.form-group').removeClass('has-error').addClass('has-success');
			repwd.closest('.form-group').find('label.hlp-msg').remove();  
			return true;
		}
	},
	hapus : function(id){
		var _this=this;
		$.post(_page_info.url+'/set_hapus',{id:id},function(respon){
			if(respon.isOk){
				_this.mdl_inp.notify('success',_page_info.title,'Pengguna berhasil dihapus.');	
				_this.dt.reload();
			}else{
				_this.mdl_inp.notify('error',_page_info.title,respon.msg);
			}
		},'json').fail(function(){
			_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		})
	},
	set_aktiftoggle_change : function(elm){
		var _this=this;
		var par=JSON.parse(urldecode($(elm).attr('data-par')));
		if($(elm).is(':checked')) var isCheck='YES'; else var isCheck='NO';
		$.post(_page_info.url+'/set_aktifchange',{id:par.id,aktif:isCheck},function(respon){
			if(respon.isOk){
				_this.mdl_inp.notify('success',_page_info.title,'Berhasil merubah status keaktifan akun pengguna.');
			}else{
				_this.mdl_inp.notify('error',_page_info.title,respon.msg);
			}
		},'json').fail(function(){
			_this.mdl_inp.notify('error',_page_info.title,_msg.unknow_error);
		})	
	}
}
$(document).ready(function(){
	adm_users.dt=$('#main_dt').my_plugins().datatable({url:'/adm_users/getdata'},['id']);
})
$('button[data-aksi="tambah"]').click(function(){
	adm_users.open_dialog_input('Tambah');
})
adm_users.mdl_inp.find('button[data-aksi="simpan"]').click(function(){
	adm_users.simpan();
})
$('body').on('change','[name="isAktif"]',function(){
	var _this;
	if($(this).is(':checked')){
		adm_users.frm_usr.find('input,select').each(function(){
			if($(this).attr('name')!='isAktif'){
				if($(this).attr('data-tmp-required')=='required'){
					$(this).attr('required','required');
					$(this).removeAttr('data-tmp-required');
				}
				$(this).closest('.form-group').show(300)
			}
		})
	}else{
		adm_users.frm_usr.find('input,select').each(function(){
			if($(this).attr('name')!='isAktif'){
				if($(this).attr('required')=='required'){
					$(this).attr('data-tmp-required','required');
					$(this).removeAttr('required');
				}
				$(this).closest('.form-group').hide(300)
			}
		})
	}
})
$('body').on('click','a[data-aksi="adm-del"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	bootbox.confirm('Apakah anda yakin akan menghapus pengguna atas nama <b>'+par.nama+'</b> ?',function(respon){
		if(respon) adm_users.hapus(par.id);
	})
})
$('body').on('change','input[name="usr_isaktif"]',function(){
	adm_users.set_aktiftoggle_change($(this));
})
$('body').on('click','a[data-aksi="adm-ubah"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	adm_users.open_ubah(par);
})
$('body').on('click','a[data-aksi="adm-profile"]',function(){
	var par=JSON.parse(urldecode($(this).attr('data-par')));
	adm_users.open_profile(par);
})