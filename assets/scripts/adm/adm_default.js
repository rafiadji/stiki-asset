$.fn.admDefaultPage = function(conf) 
{
	var e = this;
	var conf = $.extend({
		datatable : {selector : '', config: {url: ''}, param: [] }, 
		action : [],
		init: false,
	}, conf);
	
	var def_conf_action = {
		name : '',
		type : 'ajax',
		url : '',
		modal : '', // for modal
		title : '', // for modal
		confirm: false, // for ajax
		selector:'', // for toggle
		value_on: true, // for toggle
		value_off: false, // for toggle
	};
	
	var datatable;
	var modal;
	var page;
	
	// initialize page
	e.initialize = function()
	{
		// initialize datatable
		if(conf.datatable.selector)
			e.datatable = $(conf.datatable.selector, e).my_plugins().datatable(conf.datatable.config, conf.datatable.param);
		
		// initialize myplugin
		e.page = $(e.selector).my_plugins().page();
		
		// initialize action
		$.each(conf.action, function(i, curr_action)
		{
			curr_action = $.extend(def_conf_action, curr_action);
			switch(curr_action.type) 
			{
				case 'modal':
					e.initialize_modal(curr_action);
					break;
				case 'ajax':
					e.initialize_ajax(curr_action);
					break;
				case 'toggle':
					e.initialize_toggle(curr_action);
					break;
				case 'function':
					e.initialize_function(curr_action);
					break;
				case 'submit':
					e.initialize_submit(curr_action);
					break;
			}			
		})
		
		if(typeof conf.init === 'function') conf.init();
	}

	// initialize modal
	e.initialize_modal = function(act_conf)
	{
		e.modal[act_conf.name] = $(act_conf.modal).attr('modal-name',act_conf.name).my_plugins().page();
		
		e.on('click', '[data-aksi="'+act_conf.name+'"]', function (elm) 
		{
			var name = $(this).attr('data-aksi');
			var act_conf = e.get_act_config(name);
			var modal = e.modal[act_conf.name];
			var param = $(this).attr('data-par');
			var param = param ? JSON.parse(urldecode(param)) : false;
			
			var modal_title = act_conf.title
			$.each(param, function(key, val){
				modal_title = modal_title.replace('{'+key+'}', val);
			})
			
			modal.mdl_open({title: modal_title, clearbody: true});
			modal.block();
			modal.disable_btn();
			
			$.ajax({
				url: act_conf.url,
				method: 'POST',
				data: param,
				cache: false,
				success: function(response){
					$('.modal-body', modal).html(response);
					modal.unblock();
					modal.enable_btn();
				},
				error: function(response){
					modal.notify('error', _page_info.title, _msg.unknow_error);
					modal.unblock();
					modal.enable_btn();
				},
			})
		});
	}
	
	// initialize ajax
	e.initialize_ajax = function(act_conf)
	{
		e.modal[act_conf.name] = $(act_conf.modal).my_plugins().page();
		e.on('click', '[data-aksi="'+act_conf.name+'"]', function (elm) 
		{
			var curr_elm = $(this);
			var name = curr_elm.attr('data-aksi');
			var act_conf = e.get_act_config(name);
			var param = curr_elm.attr('data-par');
			var param = param ? JSON.parse(urldecode(param)) : false;
			
			if(act_conf.confirm)
			{
				var confirm_text = act_conf.confirm
				$.each(param, function(key, val){
					confirm_text = confirm_text.replace('{'+key+'}', val);
				})
				bootbox.confirm(confirm_text, function (respon) 
				{
					if (!respon) return true;
					$.ajax({
						url: act_conf.url,
						method: 'POST',
						data: param,
						dataType: 'JSON',
						success: function(response){
							if(response.success)
							{
								if(typeof act_conf.success === 'function') act_conf.success(curr_elm, response);
								e.page.notify('success', _page_info.title, response.message);
								if(typeof refresh === 'function') refresh();
							}
							else
							{
								if(typeof act_conf.error === 'function') act_conf.error(curr_elm, response);
								e.page.notify('error', _page_info.title, response.message);
							}
							
							if(e.datatable) e.datatable.reload();
						},
						error: function(response){
							if(typeof act_conf.error === 'function') act_conf.error(curr_elm, response);
							e.page.notify('error', _page_info.title, _msg.unknow_error);
						},
					})
				})
			}
			else
			{
				$.ajax({
					url: act_conf.url,
					method: 'POST',
					data: param,
					dataType: 'JSON',
					success: function(response){
						if(response.success)
						{
							if(typeof act_conf.success === 'function') act_conf.success(curr_elm, response);
							e.page.notify('success', _page_info.title, response.message);
						}
						else
						{
							if(typeof act_conf.error === 'function') act_conf.error(curr_elm, response);
							e.page.notify('error', _page_info.title, response.message);
						}
						
						if(e.datatable) e.datatable.reload();
					},
					error: function(response){
						if(typeof act_conf.error === 'function') act_conf.error(curr_elm, response);
						e.page.notify('error', _page_info.title, _msg.unknow_error);
					},
				})
			} 
		});
	}
	
	// initialize toggle
	e.initialize_toggle = function(act_conf)
	{
		var conf_name = act_conf.name;
		e.on('change', act_conf.selector, function (elm) 
		{
			var name = conf_name;
			var act_conf = e.get_act_config(name);
			var param = $(this).attr('data-par');
			param = param ? JSON.parse(urldecode(param)) : {};
			param['value'] = $(this).is(':checked') ? act_conf.value_on : act_conf.value_off;

			$.ajax({
				url: act_conf.url,
				method: 'POST',
				data: param,
				dataType: 'JSON',
				success: function(response){
					if(response.success)
						e.page.notify('success', _page_info.title, response.message);
					else
						e.page.notify('error', _page_info.title, response.message);
				},
				error: function(response){
					e.page.notify('error', _page_info.title, _msg.unknow_error);
				},
			})
		});
	}
	
	// initialize function
	e.initialize_function = function(act_conf)
	{
		var conf_name = act_conf.name;
		
		e.on('click', '[data-aksi="'+act_conf.name+'"]', function (elm) 
		{
			var name = conf_name;
			var name2 = $(this).attr('data-aksi');
			var act_conf = e.get_act_config(name);
			if (typeof act_conf.action === 'function') act_conf.action($(this), act_conf);
		});
	}
	
	// initialize submit
	e.initialize_submit = function(act_conf)
	{
		var conf_name = act_conf.name;
		e.closest('#content').on('click', '[data-aksi="'+act_conf.name+'"]', function (elm) 
		{
			var name = conf_name;
			var act_conf = e.get_act_config(name);
			var is_in_modal = $(this).parents('.modal-content').length
			var form = $(this).parents('form');
			var modal = false;
			
			
			if(!form.length && is_in_modal) form = $(this).parents('.modal-content').find('form');
			if(is_in_modal)
			{
				// search curr modal
				modal_name = $(this).parents('.modal').attr('modal-name')
				modal = e.modal[modal_name];
				modal.block();
				modal.disable_btn();
			}
			
			form.trigger('submit', {
				success: function(){
					if(modal)
					{
						modal.unblock();
						modal.enable_btn();
						modal.mdl_hide();
					}
					if(e.datatable && typeof e.datatable.reload === 'function') e.datatable.reload();
					if(typeof refresh === 'function') refresh();
				},
				error: function(){
					if(modal)
					{
						modal.unblock();
						modal.enable_btn();
					}
				},
			})
		});
	}
	
	// get action config
	e.get_act_config = function (act_name)
	{
		for(i=0; i<conf.action.length; i++) if(conf.action[i].name == act_name) return conf.action[i];
		return false;
	}
	
	
	e.initialize();
	
}
