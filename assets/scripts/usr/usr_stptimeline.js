/* STP TIMELINE */
(function ( $ ){
  $.fn.stptimeline = function(id){
    this.idstp=id;
    this.page = this.my_plugins().page();
    _this = this;
    this.open_reg = function(){
      var _this=this;
      var _panel=_this.find('div[data-dock="stp-timeline-dock-reg"]');
      _panel.attr('data-id',_this.idstp);
      _this.open_dock(_panel);
      setTimeout(function(){ 
        _dock_body=$('div[data-dock="stp-timeline-dock-reg"][data-id="'+_this.idstp+'"]').parent().attr('data-id','stp-timeline-dock-reg-'+_this.idstp);
        _dock_body=$('[data-id="stp-timeline-dock-reg-'+_this.idstp+'"]').my_plugins().page();
        _dock_body.block(); 
        $.get(_site_url+'/usr_apply_stp_kls/load_stp_lihat',{id:_this.idstp},function(respon){
          var _panel = _dock_body.find('div[data-dock="stp-timeline-dock-reg"]').my_plugins().page();
          _panel.html(respon);
          _dock_body.unblock();
        }).fail(function(){
          _dock_body.unblock();
          _dock_body.notify('error', _page_info.title, _msg.unknow_error);
        })
      }, 400);
    };
    this.open_formuji = function(){
      var _this=this;
      var _panel=_this.find('div[data-dock="stp-timeline-dock-isiuji"]');
      _panel.attr('data-id',_this.idstp);
      _this.open_dock(_panel);
      setTimeout(function(){ 
        _dock_body=$('div[data-dock="stp-timeline-dock-isiuji"][data-id="'+_this.idstp+'"]').parent().attr('data-id','stp-timeline-dock-isiuji-'+_this.idstp);
        _dock_body=$('[data-id="stp-timeline-dock-isiuji-'+_this.idstp+'"]').my_plugins().page();
        _dock_body.block(); 
        $.get(_site_url+'/usr_apply_stp_kls/load_stp_formuji',{id:_this.idstp},function(respon){
          var _panel = _dock_body.find('div[data-dock="stp-timeline-dock-isiuji"]').my_plugins().page();
          _panel.html(respon);
          _dock_body.unblock();
        }).fail(function(){
          _dock_body.unblock();
          _dock_body.notify('error', _page_info.title, _msg.unknow_error);
        })
      }, 400);
    };
    this.open_lihatuji = function(){
      var _this=this;
      var _panel=_this.find('div[data-dock="stp-timeline-dock-isiuji"]');
      _panel.attr('data-id',_this.idstp).html('');
      _this.open_dock(_panel);
      setTimeout(function(){ 
        _dock_body=$('div[data-dock="stp-timeline-dock-isiuji"][data-id="'+_this.idstp+'"]').parent().attr('data-id','stp-timeline-dock-isiuji-'+_this.idstp);
        _dock_body=$('[data-id="stp-timeline-dock-isiuji-'+_this.idstp+'"]').my_plugins().page();
        _dock_body.block(); 
        $.post(_site_url + '/usr_apply_stp_kls/get_stp_formuji_lihat', {id: _this.idstp}, function (respon) {
          var _panel = _dock_body.find('div[data-dock="stp-timeline-dock-isiuji"]').my_plugins().page();
          _panel.html(respon);
          _dock_body.unblock();
        }).fail(function(){
          _dock_body.unblock();
          _dock_body.notify('error', _page_info.title, _msg.unknow_error);
        })
      }, 400);
    };
    this.reload = function(){
      var _this=this;
      var __idstp=this.idstp;
      this.page.block();
      $.get(_site_url+'/usr_apply_stp_kls/load_timeline_stp',{openstp:this.idstp},function(respon){
        $('#tab_ongoing_'+_this.idstp).html(respon.html);
        if(typeof usr_stp.timelines == 'object') usr_stp.timelines[__idstp] = $('#stp-timeline-'+__idstp).stptimeline(__idstp);
      },'json').fail(function(){
        _this.page.unblock();
        _this.page.notify('error', _page_info.title, _msg.unknow_error);
      })
    };
    this.open_dock = function($target){
      $target.dockmodal({
        minimizedWidth: 220,
        height: 430,
        title: function() {
          return this.data('title');
        },
        initialState: "modal"
      });
    };
    this.open_formupload = function(){
      var src=_this.find('.stp-step-upload');
      if(src.length>0){
        var _panel=_this.find('div[data-dock="stp-timeline-dock-upload"]');
        _panel.attr('data-id',_this.idstp).html('');
        _this.open_dock(_panel);
        setTimeout(function(){ 
        _dock_body=$('div[data-dock="stp-timeline-dock-upload"][data-id="'+_this.idstp+'"]').parent().attr('data-id','stp-timeline-dock-upload-'+_this.idstp);
        _dock_body=$('[data-id="stp-timeline-dock-upload-'+_this.idstp+'"]').my_plugins().page();
        _dock_body.block(); 
        $.post(_site_url + '/adm_stp_permohonan/upload', {id: _this.idstp}, function (respon) {
          var _panel = _dock_body.find('div[data-dock="stp-timeline-dock-upload"]').my_plugins().page();
          _panel.html(respon);
          _dock_body.unblock();
        }).fail(function(){
          _dock_body.unblock();
          _dock_body.notify('error', _page_info.title, _msg.unknow_error);
        })
      }, 400);
      }
    };
    this.submit_upload = function(id){
      var panel=_this.find('.stp-step-upload');
      if(panel.length>0){
        _dock_body=$('[data-id="stp-timeline-dock-upload-'+_this.idstp+'"]').my_plugins().page();
        _dock_body.block();
        $.post(_site_url+'/adm_stp_permohonan/submit_berkas',{id:_this.idstp},function(respon){
          if(respon.success){
            _this.page.notify('success', _page_info.title, respon.message);
            _dock_body.unblock();
            _dock_body.closest('.dockmodal').find('a.action-close').click();
            _this.reload();
          }else{
            _dock_body.unblock();
            _this.page.notify('error', _page_info.title, respon.message);
          }
        },'json').fail(function(){
          _dock_body.unblock();
          _dock_body.notify('error', _page_info.title, _msg.unknow_error);
        })
      }
    }
    this.init = function(){
      var _nStep=this.find('ul.nav>li').length;
      var _nStepDone=this.find('ul.nav>li>a>i.fa-check.text-success').length;
      var _progBar=Math.round(_nStepDone/_nStep*100);
      _this.find('div.progress-bar').css('width',_progBar+'%').attr('aria-valuenow',_progBar).html(_progBar+'%');
    }
    this.init();
    return this;
  }
}(jQuery));

$('body').on('click','a[data-aksi="stp-timeline-lihatreg"]',function(){
  var idstp=$(this).closest('.stp-timeline').attr('data-idstp');
  usr_stp.timelines[idstp].open_reg();
})
$('body').on('click','a[data-aksi="stp-timeline-isiuji"]',function(){
  var idstp=$(this).closest('.stp-timeline').attr('data-idstp');
  usr_stp.timelines[idstp].open_formuji();
})
$('body').on('click','a[data-aksi="stp-timeline-lihatuji"]',function(){
  var idstp=$(this).closest('.stp-timeline').attr('data-idstp');
  usr_stp.timelines[idstp].open_lihatuji();
})
$('body').on('click','a[data-aksi="stp-timeline-openupload"]',function(){
  var idstp=$(this).closest('.stp-timeline').attr('data-idstp');
  usr_stp.timelines[idstp].open_formupload();
})
$('body').on('click','button[data-aksi="submit-berkas"]',function(){
  var idstp=$(this).closest('[data-dock="stp-timeline-dock-upload"]').attr('data-id');
  usr_stp.timelines[idstp].submit_upload();
})