var usr_stp = {
  tab: $('#tab_usrstp'),
  tab_info: $('#tab_guideinfo'),
  forms: {},
  timelines : {},
  init: function () {
    this.open_first_page();
    $('.syarat_stp li').tooltip({trigger: 'hover', title: 'Klik untuk informasi lebih detail', placement: 'bottom'});
    this.tab_adjust_nav_height();
  },
  open_info_syarat: function (elm) {
    this.tab.find('li a[href="#tab_info"]').trigger('click');
    this.tab_info.find('li a[href="#tab_guideinfo_syarat"]').trigger('click');
    var info_stp = '#accordion_guideinfo_syarat_' + $(elm).closest('.col-jnsstp').attr('data-id');
    if ($(info_stp).hasClass('in')) {
      //do nothing
    } else
      $('a[href="' + info_stp + '"]').trigger('click');
  },
  open_add_stp: function (par) {
    var _this = this;
    // Adding New Tab Page
    var _tabid = new Date().getUTCMilliseconds();
    this.tab.find('ul#nav-tab_usrstp').prepend('<li class=""><a href="#tab_applystp_' + _tabid + '" data-toggle="tab" aria-expanded="false"><i class="nav-icon imoon imoon-plus text-primary"></i>&nbsp;Buat ' + par.title + '</a></li>');
    this.tab.find('.tab-content#tab-content-tab_usrstp').prepend('<div id="tab_applystp_' + _tabid + '" class="tab-pane"></div>');

    this.tab_adjust_nav_height();

    // Open Created Tab
    this.tab.find('a[href="#tab_applystp_' + _tabid + '"]').trigger('click');

    $_new = this.tab.find('#tab_applystp_' + _tabid).my_plugins().page();
    $_new.block();

    // Load Form Input
    $.get(_page_info.url + '/load_form_stp', {jenis: par.id, tab: _tabid}, function (respon) {
      $_new.html(respon.html);
      _form = $('form#usr_stp' + _tabid).my_plugins().form();
      _form.setdata(respon.data);
      _this.forms['usr_stp' + _tabid] = _form;
      $('html, body').animate({scrollTop: 0}, 400);
    }, 'json').fail(function () {
      $_new.unblock();
      $_new.notify('error', _page_info.title, _msg.unknow_error);
    })
  },
  open_saved_stp: function (id,tab) {
    var _this = this;
    if(typeof tab!='undefined') $_con=$(tab);
    else $_con = this.tab.find('#tab_ongoing_' + id).my_plugins().page();
    $_con.block();

    $.get(_page_info.url + '/load_stp', {openstp: id, tab: id}, function (respon) {
      $_con.html(respon.html);
      if(respon.html_type=='stp-timeline'){
        _this.timelines[id]=$('#stp-timeline-'+id).stptimeline(id);  
      }else{
        _form = $('form#usr_stp' + id).my_plugins().form();
        if (typeof respon.data === 'object')
          _form.setdata(respon.data);
        _this.forms['usr_stp' + id] = _form;
      }
      
      _this.stp_set_btn_enable('usr_stp' + id);
      $('html, body').animate({scrollTop: 0}, 400);

      _this.tab.find('a[data-aksi="stp_open"][data-id="' + id + '"]').addClass('loaded');
    }, 'json').fail(function () {
      $_con.unblock();
      $_con.notify('error', _page_info.title, _msg.unknow_error);
    })
  },
  open_his_stp : function(id,title){
    this.tab.find('ul#nav-tab_usrstp').prepend('<li class=""><a href="#tab_ongoing_' + id + '" data-id="'+id+'" data-toggle="tab" aria-expanded="false"><i class="nav-icon imoon imoon-folder-open text-primary"></i>&nbsp; ' + title + '</a></li>');
    this.tab.find('.tab-content#tab-content-tab_usrstp').prepend('<div id="tab_ongoing_' + id + '" class="tab-pane"></div>');

    this.tab_adjust_nav_height();

    // Open Created Tab
    this.tab.find('a[href="#tab_ongoing_' + id + '"]').trigger('click');
    this.open_saved_stp(id);
  },
  close_tab_page: function (id) {
    this.tab.find('a[href="#' + id + '"]').closest('li').remove();
    this.tab.find('#' + id).remove();
    this.tab_adjust_nav_height();
    this.open_first_page();
  },
  stp_remove: function (tabid) {
    var _this = this;
    $_con = this.tab.find('#' + tabid).my_plugins().page();
    $_con.block();
    var id = _this.tab.find('a[href="#' + tabid + '"]').attr('data-id');
    $.post(_page_info.url + '/set_stpremove', {id: id}, function (respon) {
      if (respon.isOk) {
        $_con.notify('success', _page_info.title, respon.msg);
        _this.close_tab_page(tabid);
      } else {
        $_con.notify('error', _page_info.title, _msg.unknow_error);
      }
      $_con.unblock();
    }, 'json').fail(function () {
      $_con.unblock();
      $_con.notify('error', _page_info.title, _msg.unknow_error);
    })
    return true;
  },
  open_first_page: function () {
    _tab = this.tab.find('ul.nav li:first').find('a').trigger('click');
    $('html, body').animate({scrollTop: 0}, 400);
  },
  tab_adjust_nav_height: function () {
    $('#dynamic_css').html('');

    var max_hi = 0;
    this.tab.find('ul.nav li').each(function () {
      if (max_hi < $(this).find('a').height())
        max_hi = $(this).find('a').height();
    })
    max_hi = max_hi + 30;
    $('#dynamic_css').html('<style type="text/css">@media (min-width: 768px){ #tab_usrstp>ul.nav>li>a { height: ' + max_hi + 'px}}</style>');
  },
  stp_savedraft: function (tab) {
    var _this = this;
    var _tab = $('#' + tab).my_plugins().page();
    _tab.block();

    var form = _tab.find('form').attr('id');
    var data = usr_stp.forms[form].serialize();
    $.post(_page_info.url + '/set_sptsavedraft', data, function (respon) {
      if (respon.isOk) {
        _tab.addClass('stp_saved');
        _this.tab.find('a[href="#' + tab + '"]').attr('data-id', respon.id);
        _tab.find('form').find('input[name="id"]').val(respon.id);
        _tab.notify('success', _page_info.title, respon.msg);
      } else
        _tab.notify('error', _page_info.title, respon.msg);

      _tab.unblock();
    }, 'json').fail(function () {
      _tab.unblock();
      _tab.notify('error', _page_info.title, _msg.unknow_error);
    })
  },
  stp_savesubmit: function (tab) {
    var _this = this;
    var _tab = $('#' + tab).my_plugins().page();
    _tab.block();

    var form = _tab.find('form').attr('id');
    var data = usr_stp.forms[form].serialize();
    $.post(_page_info.url + '/set_stpsubmit', data, function (respon) {
      if (respon.isOk) {
        _tab.addClass('stp_saved');
        _this.tab.find('a[href="#' + tab + '"]').attr('data-id', respon.id);
        _tab.notify('success', _page_info.title, respon.msg);
        _this.open_saved_stp(respon.id,_tab);
      } else
        _tab.notify('error', _page_info.title, respon.msg);

      _tab.unblock();
    }, 'json').fail(function () {
      _tab.unblock();
      _tab.notify('error', _page_info.title, _msg.unknow_error);
    })
  },
  stp_set_btn_disable: function (form) {
    $('form#' + form).closest('div.tab-pane').find('a[data-aksi="stp_savedraft"]').addClass('disabled');
    $('form#' + form).closest('div.tab-pane').find('a[data-aksi="stp_submit"]').addClass('disabled');
  },
  stp_set_btn_enable: function (form) {
    $('form#' + form).closest('div.tab-pane').find('a[data-aksi="stp_savedraft"]').removeClass('disabled');
    $('form#' + form).closest('div.tab-pane').find('a[data-aksi="stp_submit"]').removeClass('disabled');
  },
}

$(document).ready(function () {
  usr_stp.init();
})
$('.syarat_stp li').click(function () {
  usr_stp.open_info_syarat($(this));
})
$('body').on('click', 'a[data-aksi="add_stp"]', function () {
  var col = $(this).closest('.col-jnsstp');
  var par = {
    id: col.attr('data-id'),
    title: col.find('.jnsstp-title').html()
  };
  usr_stp.open_add_stp(par);
})
$('body').on('click', 'a[data-aksi="stp_discard"]', function () {
  var tab_pane = $(this).closest('div.tab-pane');
  var form = $(this).closest('div.panel').find('form').attr('id');
  var tab = tab_pane.attr('id');
  if (tab_pane.hasClass('stp_saved')) {
    bootbox.confirm('Apakah Anda yakin mengabaikan perubahan dan membatalkan permohonan ?', function (respon) {
      if (respon)
        usr_stp.stp_remove(tab);
    })
  } else {
    if (usr_stp.forms[form].is_empty()) {
      usr_stp.close_tab_page(tab);
    } else {
      bootbox.confirm('Apakah Anda yakin mengabaikan perubahan dan membatalkan permohonan ?', function (respon) {
        if (respon)
          usr_stp.close_tab_page(tab);
      })
    }
  }
})
$('body').on('click', 'a[data-aksi="stp_savedraft"]', function () {
  usr_stp.stp_savedraft($(this).closest('div.tab-pane').attr('id'));
})
$('body').on('keyup', 'input[name="nama_lks"]', function () {
  var form = $(this).closest('form').attr('id');
  if ($(this).val().length > 0) {
    usr_stp.stp_set_btn_enable(form);
  } else {
    usr_stp.stp_set_btn_disable(form);
  }
})
$('body').on('click', 'a[data-aksi="stp_open"]', function () {
  var _this=this;
  if ($(this).hasClass('loaded')) {
    //do nothing
  } else{
    setTimeout(function(){ usr_stp.open_saved_stp($(_this).attr('data-id'));; }, 400);
  }
})
$('body').on('click', 'a[data-aksi="stp_submit"]', function () {
  var _this = this;
  var form = $(this).closest('div.tab-pane').find('form').attr('id');
  if (usr_stp.forms[form].validate()) {
    bootbox.confirm({
      message: "<p>Setelah menekan tombol simpan, Permohonan STP akan kami proses dan Anda tidak lagi dapat merubah isi permohonan. Seluruh informasi yang diberikan dalam permohonan menjadi tanggung jawab pemohon, pemohon menyatakan bahwa informasi yang diberikan adalah benar dan pemohon tunduk sepenuhnya terhadap ketentuan yang berlaku.</p>",
      buttons: {
        confirm: {
          label: '<i class="fa fa-envelope-o icon20"></i>&nbsp;Simpan &amp; Ajukan Permohonan',
          className: 'btn-success'
        },
        cancel: {
          label: 'Batal',
          className: 'btn-default'
        }
      },
      callback: function (result) {
        if (result)
          usr_stp.stp_savesubmit($(_this).closest('div.tab-pane').attr('id'));
      }
    });
  }
});