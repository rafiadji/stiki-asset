/* STP INSTRUMEN UJI */
(function ($) {
  $.fn.instrumen_uji = function () {
    var idstp = null;
    var panel = null;
    var _this = this;
    this.serialize = function () {
      var sesi = _this.find('input[name="session"]').val();
      var data = {idstp: this.idstp, sesi: sesi, uji: {}};
      this.find('.formuji-param').each(function (i, val) {
        data.uji[$(this).attr('data-item')] = $(this).val();
      })
      return data;
    }
    this.submit = function (send = false) {
      this.panel.block();
      var data = this.serialize();
      if (send)
        data.issend = true;
      $.post(_site_url + '/usr_apply_stp_kls/set_stp_formuji_submit', data, function (respon) {
        if (respon.isOk) {
          _this.panel.notify('success', _page_info.title, respon.msg);
          _this.find('input[name="session"]').val(respon.session);
          if (send) {
            _this.get_view_uji();
          }
          if (typeof usr_stp != 'undefined' && typeof usr_stp.timelines[_this.idstp] != 'undefined')
            usr_stp.timelines[_this.idstp].reload();
          if (typeof __onChanged_stpuji == 'function') __onChanged_stpuji(data);
        } else {
          _this.panel.notify('error', _page_info.title, respon.msg);
        }
        _this.panel.unblock();
      }, 'json').fail(function () {
        _this.panel.unblock();
        _this.panel.notify('error', _page_info.title, _msg.unknow_error);
      })
    }
    this.send = function () {
      if (this.validate(true)) {
        bootbox.confirm({
          message: "<p>Setelah menekan tombol simpan, Permohonan telah menyatakan segala informasi yang diisikan pada instrumen uji adalah benar dan sesuai dengan fakta di LKS. Pemohon menyatakan siap apabila Tim Dinas Sosial sewaktu-waktu melakukan kunjungan ke LKS pemohon.</p><p><strong>Setelah menekan tombol simpan, pemohon tidak dapat merubah isian kembali.</strong></p>",
          buttons: {
            confirm: {
              label: '<i class="fa fa-envelope-o icon20"></i>&nbsp;Saya Mengerti &amp; Lanjutkan',
              className: 'btn-success'
            },
            cancel: {
              label: 'Batal',
              className: 'btn-default'
            }
          },
          callback: function (result) {
            if (result)
              _this.submit(true);
          }
        });
      }
    }
    this.set_original_value = function () {
      this.find('.formuji-param').each(function (i, val) {
        $(this).val($(this).attr('data-value'));
      })
    }
    this.validate = function (display_error = false) {
      var is = true;
      this.find('.formuji-param').each(function (i, val) {
        _val = $(this).val();
        var _td = $(this).closest('td');
        if (_val == null || _val == '') {
          is = false;
          if (display_error) {
            _td.addClass('has-error');
            _td.append('<label class="text-danger">Parameter harus diisi</label>');
          }
        } else {
          if (display_error) {
            _td.removeClass('has-error');
            _td.find('label').remove();
          }
        }
      })
      if (is == false && display_error) {
        var _target = this.find('td.has-error:first');
        ;
        this.closest('.dockmodal-body').animate({
          scrollTop: $(_target).position().top
        }, 1000);
      }
      if (is)
        this.find('a[data-aksi="stp_send_uji"]').removeClass('disabled');
      else
        this.find('a[data-aksi="stp_send_uji"]').addClass('disabled');
      return is;
    }
    this.get_view_uji = function () {
      this.panel.block();
      $.post(_site_url + '/usr_apply_stp_kls/get_stp_formuji_lihat', {id: this.idstp}, function (respon) {
        _this.panel.unblock();
        _this.closest('[data-dock="stp-timeline-dock-isiuji"]').html(respon);
      }).fail(function () {
        _this.panel.unblock();
        _this.panel.notify('error', _page_info.title, _msg.unknow_error);
      })
    }
    this.init = function () {
      this.idstp = _this.closest('.pn-formuji').find('input[name="idstp"]').val();
      this.panel = this.my_plugins().page();
      this.set_original_value();
      this.validate();
    }
    this.init();
    return this;
  }
}(jQuery));