$('a[data-aksi="addrowsalespricebynumber"]').click(function(){
	addrowsalespricebynumber.addrow($(this));
})
$('form[data-context="salespricebynumber"]').on('click','a.dynamic_grow[data-aksi="remove"]',function(){
	addrowsalespricebynumber.handle_btn_remove($(this));
})
var addrowsalespricebynumber = {
	restore : function(ins){
		var _this = this;
		$frm = $(ins).closest('form');
		var _data = JSON.parse(urldecode($frm.attr('data-array')));
		if(typeof _data.data != 'undefined'){
			var _i = 0;
			$.each(_data.data,function(i,v){
				if(_i>0) _this.addrow($frm.find('a[data-aksi="addrowsalespricebynumber"]'));
				_this.handle_order(ins);

				$frm.find('[name="'+i+'"]').val(v);
				_i++;
			})
		}
		_this.init(ins);
	},
	init : function(ins){
		var _this = this;
		$frm = $(ins).closest('form');
		_this.handle_order(ins);
		$('form#'+$frm.attr('id')).my_plugins().form();
	},
	addrow : function(ins){
		var _html = urldecode($(ins).attr('data-tmplt'));
		$(ins).closest('div.form-group').before(_html);
		this.init(ins);
	},
	handle_order : function(ins){
		$frm = $(ins).closest('form');
		var number = 0;
		$frm.find('div.form-group').each(function(){
			if($(this).hasClass('dynamic_grow')){
				number = number + 1;
				$(this).find('.control-label').html($(this).find('.control-label').attr('data-raw')+number);
				$(this).find('.form-control').attr('name',$(this).find('.form-control').attr('data-rawname')+number);
			}
		})
	},
	handle_btn_remove : function(ins){
		var _ins = $(ins).closest('form').find('a[data-aksi="addrowsalespricebynumber"]');
		$(ins).closest('div.form-group').remove();
		this.handle_order(_ins);
	}
}