$('select[data-type="ref-attr_opts"]').change(function(){
  __callbackrefattropts($(this));
})
function __callbackrefattropts($ins){
  var _this = $($ins);
  var conf = JSON.parse(urldecode(_this.attr('data-config')));
  $.get(conf.url_getform,{id:_this.val(),data:_this.attr('data-array')},function(respon){
    var con = _this.closest('div.col-inp');
    con.find('form').remove();
    if(respon.isOk){
      con.append(respon.form.form);
      con.append(respon.form.js);
      $('form#'+respon.form.form_id).my_plugins().form();
      if(typeof addrowsalespricebynumber != 'undefined' && typeof addrowsalespricebynumber.restore == 'function'){
        addrowsalespricebynumber.restore($('form#'+respon.form.form_id).find('a'));
      }
    }
    
  },'json').fail(function(){
    
  })
}